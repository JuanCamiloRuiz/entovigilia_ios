//
//  RegisterDetailViewController.swift
//  EntoVigilia
//
//  Created by Juan Camilo Ruiz on 3/19/19.
//  Copyright © 2019 Juan Camilo Ruiz. All rights reserved.
//

import UIKit
import ReachabilitySwift

class RegisterDetailViewController: UIViewController {
    
    let imageView = UIImageView(image: UIImage(named: "noWifi.png"))
    let reachability = Reachability()!
    
    @IBOutlet weak var latitudLabel: UITextField!
    @IBOutlet weak var longitudLabel: UITextField!
    @IBOutlet weak var deptoLabel: UITextField!
    @IBOutlet weak var munLabel: UITextField!
    @IBOutlet weak var veredaLabel: UITextField!
    @IBOutlet weak var idLabel: UITextField!
    @IBOutlet weak var RefView: UIView!
    @IBOutlet weak var interView: UIView!
    @IBOutlet weak var periView: UIView!
    
    
    var actualRegister: Register?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadRegister()
        // Do any additional setup after loading the view.
    }
    
    private func loadRegister(){
        var referenceRegister: RegisterReference
        referenceRegister = actualRegister!.refRegister
        latitudLabel.text = String(referenceRegister.latitud)
        longitudLabel.text = String(referenceRegister.longitud)
        deptoLabel.text = referenceRegister.departamento
        munLabel.text = referenceRegister.municipio
        veredaLabel.text = referenceRegister.vereda
        idLabel.text = String(referenceRegister.id)
    }

    override func viewWillAppear(_ animated: Bool) {
        ReachabilityManager.shared.addListener(listener: self)
        if reachability.currentReachabilityStatus == Reachability.NetworkStatus.reachableViaWiFi {
            deleteWarningLabel()
        } else if reachability.currentReachabilityStatus == Reachability.NetworkStatus.reachableViaWWAN {
            deleteWarningLabel()
        } else {
            generateWarningLabel()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        ReachabilityManager.shared.removeListener(listener: self)
    }
    
    func makeRounded() {
        RefView.layer.cornerRadius = 5
        RefView.layer.shadowColor = UIColor.black.cgColor
        RefView.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        RefView.layer.masksToBounds = false
        RefView.layer.shadowRadius = 2.0
        RefView.layer.shadowOpacity = 0.5
        
        interView.layer.cornerRadius = 5
        interView.layer.shadowColor = UIColor.black.cgColor
        interView.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        interView.layer.masksToBounds = false
        interView.layer.shadowRadius = 2.0
        interView.layer.shadowOpacity = 0.5
        
        periView.layer.cornerRadius = 5
        periView.layer.shadowColor = UIColor.black.cgColor
        periView.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        periView.layer.masksToBounds = false
        periView.layer.shadowRadius = 2.0
        periView.layer.shadowOpacity = 0.5
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showResumeCocina" {
            let detailVC: CocinaDetailViewController = segue.destination as! CocinaDetailViewController
            detailVC.actualRegister = actualRegister
            detailVC.titleRegister = "Cocina"
        } else if segue.identifier == "showResumeDorm" {
            let detailVC: CocinaDetailViewController = segue.destination as! CocinaDetailViewController
            detailVC.actualRegister = actualRegister
            detailVC.titleRegister = "Dormitorio"
        } else if segue.identifier == "showResumeZona" {
            let detailVC: CocinaDetailViewController = segue.destination as! CocinaDetailViewController
            detailVC.actualRegister = actualRegister
            detailVC.titleRegister = "ZonaSocial"
        } else if segue.identifier == "showResumeGall" {
            let detailVC: CocinaDetailViewController = segue.destination as! CocinaDetailViewController
            detailVC.actualRegister = actualRegister
            detailVC.titleRegister = "Gallinero"
        } else if segue.identifier == "showResumeCorral" {
            let detailVC: CocinaDetailViewController = segue.destination as! CocinaDetailViewController
            detailVC.actualRegister = actualRegister
            detailVC.titleRegister = "Corral"
        } else if segue.identifier == "showResumeDeposito" {
            let detailVC: CocinaDetailViewController = segue.destination as! CocinaDetailViewController
            detailVC.actualRegister = actualRegister
            detailVC.titleRegister = "Deposito"
        }
    }
    
    func generateWarningLabel(){
        imageView.frame = CGRect(x: 335, y: 65, width: 30, height: 30)
        view.addSubview(imageView)
    }
    
    func deleteWarningLabel(){
        globalHomeVC!.emptyRegisters()
        imageView.removeFromSuperview()
    }

}
extension RegisterDetailViewController: NetworkStatusListener {
    
    func networkStatusDidChange(status: Reachability.NetworkStatus) {
        
        
        switch status {
        case .notReachable:
            self.generateWarningLabel()
        case .reachableViaWiFi:
            self.deleteWarningLabel()
        case .reachableViaWWAN:
            self.deleteWarningLabel()
        }
    
    }
}
