//
//  RegisterTableViewController.swift
//  EntoVigilia
//
//  Created by Juan Camilo Ruiz on 3/19/19.
//  Copyright © 2019 Juan Camilo Ruiz. All rights reserved.
//

import UIKit
import ReachabilitySwift

class RegisterTableViewController: UITableViewController {

    let imageView = UIImageView(image: UIImage(named: "noWifi.png"))
    let reachability = Reachability()!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadRegisters()
        if reachability.currentReachabilityStatus == Reachability.NetworkStatus.reachableViaWiFi {
            deleteWarningLabel()
        } else if reachability.currentReachabilityStatus == Reachability.NetworkStatus.reachableViaWWAN {
            deleteWarningLabel()
        } else {
            generateWarningLabel()
        }
        
    }
    //MARK: properties
    
    var registers = [Register]()
    
    private func loadRegisters(){
        let regs = retrieveRegisters()
        if regs != nil{
        registers = regs!
        }
    }
    
    func retrieveRegisters() -> [Register]? {
        guard let data = NSKeyedUnarchiver.unarchiveObject(withFile: Register.ArchiveURL.path) as? Data else { return nil }
        do {
            let registers = try PropertyListDecoder().decode([Register].self, from: data)
            return registers
        } catch {
            print("Retrieve Failed")
            return nil
        }
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSections: Int = 0
        if registers.count > 0
        {
            tableView.separatorStyle = .singleLine
            numOfSections            = 1
            tableView.backgroundView = nil
        }
        else
        {
            let noDataLabel: UILabel  = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text          = "No hay registros guardados localmente"
            noDataLabel.textColor     = UIColor.black
            noDataLabel.textAlignment = .center
            tableView.backgroundView  = noDataLabel
            tableView.separatorStyle  = .none
        }
        return numOfSections
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath){
        if editingStyle == .delete {
            print("Deleted")
            deleteRegister(pos: indexPath.row)
            self.tableView.reloadData()
            // self.catNames.remove(at: indexPath.row)
            // self.tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }

    func deleteRegister(pos: Int){
        var reg = registers[pos]
        var registers = retrieveRegisters()
        var newRegisters: [Register] = []
        for register in registers!{
            if reg.refRegister.id != register.refRegister.id{
                newRegisters.append(register)
            }
        }
        saveRegisters(regs: newRegisters)
    }
    
    private func saveRegisters(regs: [Register]) {
        do {
            let data = try PropertyListEncoder().encode(regs)
            let success = NSKeyedArchiver.archiveRootObject(data, toFile: Register.ArchiveURL.path)
            print(success ? "Successful save" : "Save Failed")
        } catch {
            print("Save Failed")
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        loadRegisters()
        return registers.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "RegisterTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? RegisterTableViewCell
        let register = registers[indexPath.row]
        cell!.idLabel.text = String(register.refRegister.id)
        cell!.dateLabel.text = "Fecha de creación: " + String(register.refRegister.date)
        return cell!
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showDetailRegister" {
            let detailVC: RegisterDetailViewController = segue.destination as! RegisterDetailViewController
            let indexPath = self.tableView.indexPathForSelectedRow
            let thisRegister = registers[indexPath!.row] as! Register
            detailVC.actualRegister = thisRegister
        }/*
        else if segue.identifier == "backHome" {
            let destVC: UINavigationController = segue.destination as! UINavigationController
            let secondVC: HomeViewController = destVC.viewControllers.first as! HomeViewController
            secondVC.messageShowed = true
        }*/
    }
    
    override func viewWillAppear(_ animated: Bool) {
        ReachabilityManager.shared.addListener(listener: self)
        if reachability.currentReachabilityStatus == Reachability.NetworkStatus.reachableViaWiFi {
            deleteWarningLabel()
        } else if reachability.currentReachabilityStatus == Reachability.NetworkStatus.reachableViaWWAN {
            deleteWarningLabel()
        } else {
            generateWarningLabel()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        ReachabilityManager.shared.removeListener(listener: self)
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func generateWarningLabel(){
        var headerView: UIView = UIView.init(frame: CGRect(origin: CGPoint(x: 1,y :50), size: CGSize(width: 276, height: 30)))
        imageView.frame = CGRect(x: 335, y: 0, width: 30, height: 30)
        headerView.addSubview(imageView)
        self.tableView.tableHeaderView = headerView
        /*
        warningLabel.center = CGPoint(x: 190, y: 15)
        warningLabel.textAlignment = .center
        warningLabel.text = "No hay conexión a internet"
        warningLabel.backgroundColor = UIColor.red
        warningLabel.layer.cornerRadius = 50
        self.view.addSubview(warningLabel)
         */
    }
    
    func deleteWarningLabel(){
        self.tableView.tableHeaderView = nil
        globalHomeVC!.emptyRegisters()
    }

}
extension RegisterTableViewController: NetworkStatusListener {
    
    func networkStatusDidChange(status: Reachability.NetworkStatus) {
        
        switch status {
        case .notReachable:
            self.generateWarningLabel()
        case .reachableViaWiFi:
            self.deleteWarningLabel()
        case .reachableViaWWAN:
            self.deleteWarningLabel()
        }
         
    }
}
