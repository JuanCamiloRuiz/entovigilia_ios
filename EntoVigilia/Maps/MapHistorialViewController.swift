//
//  MapHistorialViewController.swift
//  EntoVigilia
//
//  Created by Juan Camilo Ruiz on 3/30/19.
//  Copyright © 2019 Juan Camilo Ruiz. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import ReachabilitySwift
import Firebase

class MapHistorialViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {

    
    @IBOutlet weak var mapView: MKMapView!
    
    let regionRadius: CLLocationDistance = 1000
    var locationManager: CLLocationManager = CLLocationManager()
    var initialLocation: CLLocation!
    var firstLoad = false
    var idLocation: String = ""
    private var roundButton = UIButton()
    private var statsButton = UIButton()
    let reachability = Reachability()!
    let imageView = UIImageView(image: UIImage(named: "noWifi.png"))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //createFloatingButton()
        createStatsButton()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
        initialLocation = nil
        locationManager.stopUpdatingLocation()
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
        loadPins()
        
        mapView.delegate = self
    }
    
    func createStatsButton() {
        statsButton = UIButton(type: .custom)
        statsButton.translatesAutoresizingMaskIntoConstraints = false
        // statsButton.backgroundColor = UIColor(red: 11/255, green: 201/255, blue: 8/255, alpha: 1.0)
        // Make sure you replace the name of the image:
        statsButton.setImage(UIImage(named:"statsIcon"), for: .normal)
        // Make sure to create a function and replace DOTHISONTAP with your own function:
        statsButton.addTarget(self, action: #selector(goStats), for: UIControl.Event.touchUpInside)
        // We're manipulating the UI, must be on the main thread:
        DispatchQueue.main.async {
            if let keyWindow = UIApplication.shared.keyWindow {
                keyWindow.addSubview(self.statsButton)
                NSLayoutConstraint.activate([
                    keyWindow.trailingAnchor.constraint(equalTo: self.statsButton.trailingAnchor, constant: 15),
                    keyWindow.bottomAnchor.constraint(equalTo: self.statsButton.bottomAnchor, constant: 600),
                    self.statsButton.widthAnchor.constraint(equalToConstant: 50),
                    self.statsButton.heightAnchor.constraint(equalToConstant: 50)])
            }
            // Make the button round:
            self.statsButton.layer.cornerRadius = 26
            // Add a black shadow:
            self.statsButton.layer.shadowColor = UIColor.black.cgColor
            self.statsButton.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
            self.statsButton.layer.masksToBounds = false
            self.statsButton.layer.shadowRadius = 2.0
            self.statsButton.layer.shadowOpacity = 0.5
            // Add a pulsing animation to draw attention to button:
            let scaleAnimation: CABasicAnimation = CABasicAnimation(keyPath: "transform.scale")
            scaleAnimation.duration = 0.4
            scaleAnimation.repeatCount = .greatestFiniteMagnitude
            scaleAnimation.autoreverses = true
            scaleAnimation.fromValue = 1.0;
            scaleAnimation.toValue = 1.0005;
            self.statsButton.layer.add(scaleAnimation, forKey: "scale")
            self.statsButton.showsTouchWhenHighlighted = true
        }
    }
    
    @objc func goStats(){
        performSegue(withIdentifier: "goToStats", sender: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        ReachabilityManager.shared.addListener(listener: self)
        if reachability.currentReachabilityStatus == Reachability.NetworkStatus.reachableViaWiFi {
            deleteWarningLabel()
        } else if reachability.currentReachabilityStatus == Reachability.NetworkStatus.reachableViaWWAN {
            deleteWarningLabel()
        } else {
            generateWarningLabel()
        }
        
        createStatsButton()
        //createFloatingButton()
    }
    
    func generateWarningLabel(){
        imageView.frame = CGRect(x: 335, y: 65, width: 30, height: 30)
        view.addSubview(imageView)
    }
    
    func deleteWarningLabel(){
        imageView.removeFromSuperview()
    }
    
    func removeInvalidChars(str: String) -> String {
        return str.replacingOccurrences(of: ".", with: "", options: NSString.CompareOptions.literal, range: nil).replacingOccurrences(of: "@", with: "", options: NSString.CompareOptions.literal, range: nil).replacingOccurrences(of: "#", with: "", options: NSString.CompareOptions.literal, range: nil).replacingOccurrences(of: "$", with: "", options: NSString.CompareOptions.literal, range: nil).replacingOccurrences(of: "[", with: "", options: NSString.CompareOptions.literal, range: nil).replacingOccurrences(of: "]", with: "", options: NSString.CompareOptions.literal, range: nil)
    }
    
    func showNotAbleToDrawPinsMessage(){
        let alert = UIAlertController(title: "Error al cargar algunos puntos", message: "Algunos puntos del mapa no pudieron ser pintados debido a inconsistencias en los datos", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func loadPins(){
        UserDefaults.standard.removeObject(forKey: "locationArray")
        if Auth.auth().currentUser != nil{
            let username = removeInvalidChars(str: (Auth.auth().currentUser?.email!)!)
        let ref = Database.database().reference()
            ref.child("usuarios").child(username).observe(.childAdded, with: { (snapshot) in
            if let getData = snapshot.value as? [String:Any] {
                if let latitud = getData["latitud"] as? Double, let longitud = getData["longitud"] as? Double, let idHogar = getData["idHogar"] as? String, let depto = getData["departamento"] as? String, let municipio = getData["municipio"] as? String, let fecha = getData["fecha"] as? String {
                    let center = CLLocationCoordinate2D(latitude: latitud as! CLLocationDegrees, longitude: longitud as! CLLocationDegrees)
                    let title = idHogar
                    let annotation = MKPointAnnotation()
                    annotation.coordinate = center
                    annotation.title = title as! String
                    annotation.subtitle = "latitud: " + String(String(center.latitude).prefix(7)) + " longitud: " +  String(String(center.longitude).prefix(7))
                    self.mapView.addAnnotation(annotation)
                    //Now save the registers locally so the table history doesn't have to use firebase again
                    let locationDictionary:[String:Any] = ["id":idHogar,"latitud":(latitud) as! CLLocationDegrees,"longitud":(longitud) as! CLLocationDegrees, "departamento" : depto , "municipio": municipio , "fecha":fecha]
                    var locationArray = [[String:Any]]()
                    if UserDefaults.standard.object(forKey: "locationArray") != nil {
                        locationArray = UserDefaults.standard.object(forKey: "locationArray") as! [[String:Any]]
                    }
                    locationArray.append(locationDictionary)
                    UserDefaults.standard.set(locationArray, forKey: "locationArray")
                    UserDefaults.standard.synchronize()
                } else {
                    self.showNotAbleToDrawPinsMessage()
                }
               
                }
            }
        )}
    }
        /*
        if UserDefaults.standard.object(forKey: "locationArray") != nil {
            for dictionary in UserDefaults.standard.object(forKey: "locationArray") as! [[String:Any]]{
                let center = CLLocationCoordinate2D(latitude: dictionary["latitude"]! as! CLLocationDegrees, longitude: dictionary["longitude"]! as! CLLocationDegrees)
                let title = dictionary["id"]
                let annotation = MKPointAnnotation()
                annotation.coordinate = center
                annotation.title = title as! String
                annotation.subtitle = "latitud: " + String(String(center.latitude).prefix(7)) + " longitud: " +  String(String(center.longitude).prefix(7))
                self.mapView.addAnnotation(annotation)
            }
         */
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        let latesLocation: CLLocation
        latesLocation = locations[locations.count - 1]
        initialLocation = CLLocation(latitude: latesLocation.coordinate.latitude, longitude: latesLocation.coordinate.longitude)
        if !firstLoad{
            centerMapOnLocation(location: initialLocation)
            firstLoad = true
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if roundButton.superview != nil {
            DispatchQueue.main.async {
                self.roundButton.removeFromSuperview()
                self.roundButton.isEnabled = false
                self.roundButton.isHidden = true
            }
        }
        if statsButton.superview != nil {
            DispatchQueue.main.async {
                self.statsButton.removeFromSuperview()
                self.statsButton.isEnabled = false
                self.statsButton.isHidden = true
            }
        }
        ReachabilityManager.shared.removeListener(listener: self)
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate,
                                                  latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func createFloatingButton() {
        roundButton = UIButton(type: .custom)
        roundButton.translatesAutoresizingMaskIntoConstraints = false
        roundButton.backgroundColor = UIColor(red: 11/255, green: 201/255, blue: 8/255, alpha: 1.0)
        // Make sure you replace the name of the image:
        roundButton.setImage(UIImage(named:"garbage"), for: .normal)
        // Make sure to create a function and replace DOTHISONTAP with your own function:
        roundButton.addTarget(self, action: #selector(displayCreatedMessage), for: UIControl.Event.touchUpInside)
        // We're manipulating the UI, must be on the main thread:
        DispatchQueue.main.async {
            if let keyWindow = UIApplication.shared.keyWindow {
                keyWindow.addSubview(self.roundButton)
                NSLayoutConstraint.activate([
                    keyWindow.trailingAnchor.constraint(equalTo: self.roundButton.trailingAnchor, constant: 15),
                    keyWindow.bottomAnchor.constraint(equalTo: self.roundButton.bottomAnchor, constant: 50),
                    self.roundButton.widthAnchor.constraint(equalToConstant: 50),
                    self.roundButton.heightAnchor.constraint(equalToConstant: 50)])
            }
            // Make the button round:
            self.roundButton.layer.cornerRadius = 26
            // Add a black shadow:
            self.roundButton.layer.shadowColor = UIColor.black.cgColor
            self.roundButton.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
            self.roundButton.layer.masksToBounds = false
            self.roundButton.layer.shadowRadius = 2.0
            self.roundButton.layer.shadowOpacity = 0.5
            // Add a pulsing animation to draw attention to button:
            let scaleAnimation: CABasicAnimation = CABasicAnimation(keyPath: "transform.scale")
            scaleAnimation.duration = 0.4
            scaleAnimation.repeatCount = .greatestFiniteMagnitude
            scaleAnimation.autoreverses = true
            scaleAnimation.fromValue = 1.0;
            scaleAnimation.toValue = 1.0005;
            self.roundButton.layer.add(scaleAnimation, forKey: "scale")
            self.roundButton.showsTouchWhenHighlighted = true
        }
    }
    
    func deleteHistorial(){
        mapView.removeAnnotations(mapView.annotations)
        UserDefaults.standard.removeObject(forKey: "locationArray")
        loadPins()
    }
    
     @objc func displayCreatedMessage(){
        let userMessage: String = "¿Estas seguro que deseas eliminar todo el historial?"
        let myAlert = UIAlertController(title: "¿Eliminar historial?", message: userMessage, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "Si, estoy seguro", style: UIAlertAction.Style.default, handler: {_ in
            self.deleteHistorial()
        })
        let cancelAction = UIAlertAction(title: "No, cancelar", style: .cancel) { (action:UIAlertAction!) in
        }
        myAlert.addAction(cancelAction)
        myAlert.addAction(okAction)
        self.present(myAlert, animated: true, completion: nil)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "pin")
        annotationView.canShowCallout = true
        let pinImage = UIImage(named: "greenPin.png")
        let size = CGSize(width: 50, height: 50)
        UIGraphicsBeginImageContext(size)
        pinImage!.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
        
        annotationView.image = resizedImage
        
        let rightButton: AnyObject! = UIButton(type: UIButton.ButtonType.detailDisclosure)
        annotationView.rightCalloutAccessoryView = rightButton as? UIView
        return annotationView
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension MapHistorialViewController: NetworkStatusListener {
    
    func networkStatusDidChange(status: Reachability.NetworkStatus) {
        
        switch status {
        case .notReachable:
            self.generateWarningLabel()
        case .reachableViaWiFi:
            self.deleteWarningLabel()
        case .reachableViaWWAN:
            self.deleteWarningLabel()
        }
        
    }
}
