//
//  HistorialTableViewCell.swift
//  EntoVigilia
//
//  Created by Juan Camilo Ruiz on 4/24/19.
//  Copyright © 2019 Juan Camilo Ruiz. All rights reserved.
//

import UIKit

class HistorialTableViewCell: UITableViewCell {

    
    @IBOutlet weak var nombreRegistro: UILabel!
    @IBOutlet weak var fechaRegistro: UILabel!
    @IBOutlet weak var deptoRegistro: UILabel!
    @IBOutlet weak var munRegistro: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
