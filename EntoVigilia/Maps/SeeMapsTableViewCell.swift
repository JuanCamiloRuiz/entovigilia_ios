//
//  SeeMapsTableViewCell.swift
//  EntoVigilia
//
//  Created by Juan Camilo Ruiz on 4/23/19.
//  Copyright © 2019 Juan Camilo Ruiz. All rights reserved.
//

import UIKit

class SeeMapsTableViewCell: UITableViewCell {

    @IBOutlet weak var mapNameLabel: UILabel!
    @IBOutlet weak var verDetallesButton: UIButton!
    @IBOutlet weak var elegirButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
}
