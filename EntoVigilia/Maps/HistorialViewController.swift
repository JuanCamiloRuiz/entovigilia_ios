//
//  HistorialViewController.swift
//  EntoVigilia
//
//  Created by Juan Camilo Ruiz on 4/24/19.
//  Copyright © 2019 Juan Camilo Ruiz. All rights reserved.
//

import UIKit
import FirebaseDatabase
import ReachabilitySwift

class HistorialViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var numRegistrosTomados: UITextField!
    var registros = [pinRegister]()
    let reachability = Reachability()!
    let imageView = UIImageView(image: UIImage(named: "noWifi.png"))
    private var roundButton = UIButton()
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return registros.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSections: Int = 0
        if registros.count > 0
        {
            tableView.separatorStyle = .singleLine
            numOfSections            = 1
            tableView.backgroundView = nil
        }
        else
        {
            let noDataLabel: UILabel  = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text          = "No hay registros en el historial"
            noDataLabel.textColor     = UIColor.black
            noDataLabel.textAlignment = .center
            tableView.backgroundView  = noDataLabel
            tableView.separatorStyle  = .none
        }
        return numOfSections
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "HistorialTableCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? HistorialTableViewCell
        let reg = registros[indexPath.row]
        cell!.nombreRegistro.text = reg.id
        cell!.deptoRegistro.text = reg.departamento
        cell!.munRegistro.text = reg.municipio
        cell!.fechaRegistro.text = reg.fecha
        return cell!
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        loadRegisters()
        
        if reachability.currentReachabilityStatus == Reachability.NetworkStatus.reachableViaWiFi {
            deleteWarningLabel()
        } else if reachability.currentReachabilityStatus == Reachability.NetworkStatus.reachableViaWWAN {
            deleteWarningLabel()
        } else {
            generateWarningLabel()
        }
        
        numRegistrosTomados.text = String(registros.count)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        ReachabilityManager.shared.addListener(listener: self)
        if reachability.currentReachabilityStatus == Reachability.NetworkStatus.reachableViaWiFi {
            deleteWarningLabel()
        } else if reachability.currentReachabilityStatus == Reachability.NetworkStatus.reachableViaWWAN {
            deleteWarningLabel()
        } else {
            generateWarningLabel()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
         ReachabilityManager.shared.removeListener(listener: self)
    }
    
    func loadRegisters(){
        if UserDefaults.standard.object(forKey: "locationArray") != nil {
            for dictionary in UserDefaults.standard.object(forKey: "locationArray") as! [[String:Any]]{
                
                var reg = pinRegister(id: dictionary["id"] as! String, latitud: dictionary["latitud"] as! Double, longitud: dictionary["longitud"] as! Double, departamento: dictionary["departamento"]  as! String, municipio: dictionary["municipio"]  as! String, fecha: dictionary["fecha"] as! String)
                self.registros.append(reg)
            }
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/LLLL/yyyy HH:mm"
        self.registros.sort(by: { dateFormatter.date(from: $0.fecha)! > dateFormatter.date(from: $1.fecha)! })
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func generateWarningLabel(){
        imageView.frame = CGRect(x: 335, y: 65, width: 30, height: 30)
        view.addSubview(imageView)
    }
    
    func deleteWarningLabel(){
        globalHomeVC!.emptyRegisters()
        imageView.removeFromSuperview()
    }
    
}
extension HistorialViewController: NetworkStatusListener {
    
    func networkStatusDidChange(status: Reachability.NetworkStatus) {
        
        switch status {
        case .notReachable:
            self.generateWarningLabel()
        case .reachableViaWiFi:
            self.deleteWarningLabel()
        case .reachableViaWWAN:
            self.deleteWarningLabel()
        }
        
    }
}
