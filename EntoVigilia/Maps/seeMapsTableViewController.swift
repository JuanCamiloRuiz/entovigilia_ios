//
//  seeMapsTableViewController.swift
//  EntoVigilia
//
//  Created by Juan Camilo Ruiz on 4/23/19.
//  Copyright © 2019 Juan Camilo Ruiz. All rights reserved.
//

import UIKit
import ReachabilitySwift
import FirebaseDatabase
import Firebase

class seeMapsTableViewController: UITableViewController {

    let imageView = UIImageView(image: UIImage(named: "noWifi.png"))
    let reachability = Reachability()!
    var mapas = [String]()
    var loadded = false
    private var roundButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createFloatingButton()
        if !loadded {
            loadMaps()
            loadded = true
        }
        
        if reachability.currentReachabilityStatus == Reachability.NetworkStatus.reachableViaWiFi {
            deleteWarningLabel()
        } else if reachability.currentReachabilityStatus == Reachability.NetworkStatus.reachableViaWWAN {
            deleteWarningLabel()
        } else {
            generateWarningLabel()
        }
    }
    
    func createFloatingButton() {
        roundButton = UIButton(type: .custom)
        roundButton.translatesAutoresizingMaskIntoConstraints = false
        roundButton.backgroundColor = UIColor(red: 11/255, green: 201/255, blue: 8/255, alpha: 1.0)
        // Make sure you replace the name of the image:
        roundButton.setImage(UIImage(named:"mapIcon"), for: .normal)
        // Make sure to create a function and replace DOTHISONTAP with your own function:
        roundButton.addTarget(self, action: #selector(createMap), for: UIControl.Event.touchUpInside)
        // We're manipulating the UI, must be on the main thread:
        DispatchQueue.main.async {
            if let keyWindow = UIApplication.shared.keyWindow {
                keyWindow.addSubview(self.roundButton)
                NSLayoutConstraint.activate([
                    keyWindow.trailingAnchor.constraint(equalTo: self.roundButton.trailingAnchor, constant: 15),
                    keyWindow.bottomAnchor.constraint(equalTo: self.roundButton.bottomAnchor, constant: 30),
                    self.roundButton.widthAnchor.constraint(equalToConstant: 50),
                    self.roundButton.heightAnchor.constraint(equalToConstant: 50)])
            }
            // Make the button round:
            self.roundButton.layer.cornerRadius = 26
            // Add a black shadow:
            self.roundButton.layer.shadowColor = UIColor.black.cgColor
            self.roundButton.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
            self.roundButton.layer.masksToBounds = false
            self.roundButton.layer.shadowRadius = 2.0
            self.roundButton.layer.shadowOpacity = 0.5
            // Add a pulsing animation to draw attention to button:
            let scaleAnimation: CABasicAnimation = CABasicAnimation(keyPath: "transform.scale")
            scaleAnimation.duration = 0.4
            scaleAnimation.repeatCount = .greatestFiniteMagnitude
            scaleAnimation.autoreverses = true
            scaleAnimation.fromValue = 1.0;
            scaleAnimation.toValue = 1.0005;
            self.roundButton.layer.add(scaleAnimation, forKey: "scale")
            self.roundButton.showsTouchWhenHighlighted = true
        }
    }
    
    @objc func createMap(){
        if Auth.auth().currentUser != nil{
            if Auth.auth().currentUser!.isEmailVerified{
                performSegue(withIdentifier: "createMap", sender: nil)
            } else {
                displayEmailNotVerifydMessage(email: (Auth.auth().currentUser?.email)!)
            }
        } else {
            displayNotLoggedInMessage()
        }
    }
    
    func loadMaps() {
        let ref = Database.database().reference()
        ref.child("mapas").observe(.childAdded, with: { (snapshot) in
            if let getData = snapshot.value as? [String:Any] {
            do{
                if getData["nombre"] != nil {
                let mapName = getData["nombre"] as! String
                self.mapas.append(mapName)
                DispatchQueue.main.async { self.tableView.reloadData() }
                }
            } catch {
                print("Error leyendo los mapas")
                }
                
            }
        }
        )
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSections: Int = 0
        if mapas.count > 0
        {
            tableView.separatorStyle = .singleLine
            numOfSections            = 1
            tableView.backgroundView = nil
        }
        else
        {
            let noDataLabel: UILabel  = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text          = "No hay mapas creados"
            noDataLabel.textColor     = UIColor.black
            noDataLabel.textAlignment = .center
            tableView.backgroundView  = noDataLabel
            tableView.separatorStyle  = .none
        }
        return numOfSections
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mapas.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "MapTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? SeeMapsTableViewCell
        let map = mapas[indexPath.row]
        cell!.mapNameLabel.text = "Nombre del mapa: " + String(map)
        cell!.verDetallesButton.tag = indexPath.row
        cell!.elegirButton.tag = indexPath.row
        return cell!
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showMapPins" {
            if Auth.auth().currentUser != nil{
                if Auth.auth().currentUser!.isEmailVerified{
                    let detailVC: MapPinsViewController = segue.destination as! MapPinsViewController
                    let button = sender as! UIButton
                    let indexPath = button.tag
                    let thisMap = mapas[indexPath] as! String
                    detailVC.actualMap = thisMap
                } else {
                    displayEmailNotVerifydMessage(email: (Auth.auth().currentUser?.email)!)
                }
            } else {
                displayNotLoggedInMessage()
            }
        }
         else if segue.identifier == "goBackToHome" {
            let button = sender as! UIButton
            let indexPath = button.tag
            let thisMap = mapas[indexPath] as! String
            updateSelectedMap(mapName: thisMap)
        } else if segue.identifier == "createMap"  {
            let detailVC: MapPinsViewController = segue.destination as! MapPinsViewController
            detailVC.isCreatingMap = true
        }
    }
    
    func updateSelectedMap(mapName: String){
        let mapNameDict:[String:Any] = ["name" : mapName]
        var mapNameArray = [[String:Any]]()
        mapNameArray.append(mapNameDict)
        UserDefaults.standard.set(mapNameArray, forKey: "mapName")
        UserDefaults.standard.synchronize()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        createFloatingButton()
        ReachabilityManager.shared.addListener(listener: self)
        if reachability.currentReachabilityStatus == Reachability.NetworkStatus.reachableViaWiFi {
            deleteWarningLabel()
        } else if reachability.currentReachabilityStatus == Reachability.NetworkStatus.reachableViaWWAN {
            deleteWarningLabel()
        } else {
            generateWarningLabel()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        ReachabilityManager.shared.removeListener(listener: self)
        if roundButton.superview != nil {
            DispatchQueue.main.async {
                self.roundButton.removeFromSuperview()
                self.roundButton.isEnabled = false
                self.roundButton.isHidden = true
            }
        }
    }
    
    func displayEmailNotVerifydMessage(email: String){
        let userMessage: String = " El email " + email + " no ha sido verificado, por tanto no se pueden consultas ni crear mapas"
        let myAlert = UIAlertController(title: "Email no verificado", message: userMessage, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {_ in })
        myAlert.addAction(okAction)
        self.present(myAlert, animated: true, completion: nil)
    }
    
    func displayNotLoggedInMessage(){
        let userMessage: String = " No estas en ninguna cuenta de usuario, por tanto no puedes consultar ni crear mapas"
        let myAlert = UIAlertController(title: "No estas registrado", message: userMessage, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {_ in })
        myAlert.addAction(okAction)
        self.present(myAlert, animated: true, completion: nil)
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    func generateWarningLabel(){
        var headerView: UIView = UIView.init(frame: CGRect(origin: CGPoint(x: 1,y :50), size: CGSize(width: 276, height: 30)))
        imageView.frame = CGRect(x: 335, y: 0, width: 30, height: 30)
        headerView.addSubview(imageView)
        self.tableView.tableHeaderView = headerView
        /*
         warningLabel.center = CGPoint(x: 190, y: 15)
         warningLabel.textAlignment = .center
         warningLabel.text = "No hay conexión a internet"
         warningLabel.backgroundColor = UIColor.red
         warningLabel.layer.cornerRadius = 50
         self.view.addSubview(warningLabel)
         */
    }
    
    func deleteWarningLabel(){
        self.tableView.tableHeaderView = nil
    }
    
}
extension seeMapsTableViewController: NetworkStatusListener {
    
    func networkStatusDidChange(status: Reachability.NetworkStatus) {
        
        switch status {
        case .notReachable:
            self.generateWarningLabel()
        case .reachableViaWiFi:
            self.deleteWarningLabel()
        case .reachableViaWWAN:
            self.deleteWarningLabel()
        }
        
    }
}
