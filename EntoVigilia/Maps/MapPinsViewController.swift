//
//  MapViewController.swift
//  EntoVigilia
//
//  Created by Juan Camilo Ruiz on 3/28/19.
//  Copyright © 2019 Juan Camilo Ruiz. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import FirebaseDatabase
import ReachabilitySwift

class MapPinsViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate, UITextFieldDelegate{

    @IBOutlet weak var mapView: MKMapView!
    var refRegistros: DatabaseReference?
    let regionRadius: CLLocationDistance = 1000
    var locationManager: CLLocationManager = CLLocationManager()
    var initialLocation: CLLocation!
    var firstLoad = false
    var idLocation: String = ""
    let reachability = Reachability()!
    let imageView = UIImageView(image: UIImage(named: "noWifi.png"))
    var actualMap: String = ""
    var isCreatingMap = false
    var mapCreatingName: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.delegate = self
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
        initialLocation = nil
        locationManager.stopUpdatingLocation()
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
        let gestureRecognizer = UILongPressGestureRecognizer(target: self, action:(#selector(longPress)))
        gestureRecognizer.minimumPressDuration = 2.0
        gestureRecognizer.delegate = self as? UIGestureRecognizerDelegate
        mapView.addGestureRecognizer(gestureRecognizer)
        
        setUp()
        
        refRegistros = Database.database().reference()
        refRegistros!.keepSynced(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        ReachabilityManager.shared.addListener(listener: self)
        if reachability.currentReachabilityStatus == Reachability.NetworkStatus.reachableViaWiFi {
            deleteWarningLabel()
        } else if reachability.currentReachabilityStatus == Reachability.NetworkStatus.reachableViaWWAN {
            deleteWarningLabel()
        } else {
            generateWarningLabel()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        ReachabilityManager.shared.removeListener(listener: self)
        let allAnnotations = mapView.annotations
        mapView.removeAnnotations(allAnnotations)
    }
    
    func generateWarningLabel(){
        imageView.frame = CGRect(x: 335, y: 65, width: 30, height: 30)
        view.addSubview(imageView)
    }
    
    func deleteWarningLabel(){
        imageView.removeFromSuperview()
    }
    
    func setUp(){
        if isCreatingMap{
            showCreateMapMessage()
        }
        else {
        let ref = Database.database().reference()
        ref.child("mapas").child(actualMap).child("hogares").observe(.childAdded, with: { (snapshot) in
            if let getData = snapshot.value as? [String:Any], let registro = getData["registrado"] as? Int, let latitud = getData["latitud"] as? Double, let longitud = getData["longitud"] as? Double, let idHogar = getData["idHogar"] as? String{
                            self.createPin(registrado: registro, latitud: latitud, longitud: longitud, idHogar: idHogar)
            } else {
                self.showNotAbleToDrawPinsMessage()
            }
        })
        }
    }
    
    func showNotAbleToDrawPinsMessage(){
        let alert = UIAlertController(title: "Error al cargar algunos puntos", message: "Algunos puntos del mapa no pudieron ser pintados debido a inconsistencias en los datos", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showCreateMapMessage(){
        var textField = UITextField()
        let alert = UIAlertController(title: "Crear nuevo mapa", message: "Ingresa el nombre que deseas darle al nuevo mapa", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel) { (action:UIAlertAction!) in
            let VC = self.storyboard?.instantiateViewController(withIdentifier: "homeNav") as! UINavigationController
            self.present(VC, animated: true, completion: nil)
        }
        let addItemAction = UIAlertAction(title: "Crear mapa", style: .default) { (action) in
            // after user click "add item", the new item will be displayed to table view
            print(textField.text)  //Here you will get user text.
            self.mapCreatingName = textField.text!
            self.showChooseCenterMessage()
        }
        alert.addTextField { (alertTextfield) in
            alertTextfield.placeholder = "Nombre del nuevo Mapa"
            alertTextfield.delegate = self
            textField = alertTextfield
            // store a data from user to 'textField' variable, so it can be accessed in closure at 'addItemAction'
        }
        alert.addAction(addItemAction)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
        
    }
    
    func showChooseCenterMessage(){
        let alert = UIAlertController(title: "Ahora elige el centro del mapa", message: "Elige presionando por 5 segundos el lugar que deseas que sea el centro del nuevo mapa", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func createPin(registrado: Int, latitud: Double, longitud: Double, idHogar: String){
        let annotation = MKPointAnnotation()
        do {
        annotation.coordinate = CLLocationCoordinate2D(latitude: latitud , longitude: longitud )
        annotation.title = idHogar
        if registrado == 1{
            annotation.subtitle = "Visitado - latitud: " + String(String(latitud).prefix(7)) + " longitud: " +  String(String(longitud).prefix(7))
        } else {
            annotation.subtitle = "No visitado - latitud: " + String(String(latitud).prefix(7)) + " longitud: " +  String(String(longitud).prefix(7))
        }
        mapView.addAnnotation(annotation)
        } catch {
            print("No se pudo pintar un pin en el mapa")
        }
    }
    
    @objc func longPress(gestureRecognizer: UILongPressGestureRecognizer) {
        if !isCreatingMap {
        let touchPoint: CGPoint = gestureRecognizer.location(in: mapView)
        let coordinate: CLLocationCoordinate2D = mapView.convert(touchPoint, toCoordinateFrom: mapView)
        let annotation = MKPointAnnotation()
        var textField = UITextField()
        annotation.coordinate = coordinate
        let alert = UIAlertController(title: "Crear registro nuevo", message: "", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel) { (action:UIAlertAction!) in
        }
        let addItemAction = UIAlertAction(title: "Crear registro", style: .default) { (action) in
            // after user click "add item", the new item will be displayed to table view
            print(textField.text)  //Here you will get user text.
            annotation.title = textField.text
            self.uploadToFireBase(id: annotation.title!, latitud: coordinate.latitude, longitud: coordinate.longitude)
            self.mapView.addAnnotation(annotation)
        }
        alert.addTextField { (alertTextfield) in
            alertTextfield.placeholder = "ID del registro"
            alertTextfield.delegate = self
            textField = alertTextfield
            // store a data from user to 'textField' variable, so it can be accessed in closure at 'addItemAction'
        }
        alert.addAction(addItemAction)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
        annotation.subtitle = "No visitado - latitud: " + String(String(coordinate.latitude).prefix(7)) + " longitud: " +  String(String(coordinate.longitude).prefix(7))
        } else {
            let touchPoint: CGPoint = gestureRecognizer.location(in: mapView)
            let coordinate: CLLocationCoordinate2D = mapView.convert(touchPoint, toCoordinateFrom: mapView)
            showCenterChoosedMessage(lat: coordinate.latitude , lon: coordinate.longitude)
        }
    }
    
    func showCenterChoosedMessage(lat: Double, lon: Double){
        var msg = "Has elegido para el nuevo mapa: \n Nombre: " + String(mapCreatingName) + " \n Latitud: " + String(lat) + " \n Longitud: " + String(lon)
        let alert = UIAlertController(title: "¿Crear mapa con estos datos?", message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Crear nuevo mapa", style: UIAlertAction.Style.default, handler: {_ in
            self.sendMapToFireBase(mapName: self.mapCreatingName, lat: lat, lon: lon)
            let VC = self.storyboard?.instantiateViewController(withIdentifier: "homeNav") as! UINavigationController
            self.present(VC, animated: true, completion: nil)
        })
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel) { (action:UIAlertAction!) in}
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func sendMapToFireBase(mapName: String, lat: Double, lon: Double){
        print("Arrived to firebase")
        let center = [
            "latitud":Double(lat),
            "longitud":Double(lon),
        ]
        let newMap = [
            "nombre": mapName,
            "centro": center
            ] as [String : Any]
        print("Created json")
        self.refRegistros?.child("mapas").child((mapName)).setValue(newMap)
        print("Sended to firebase")
    }
    
    func uploadToFireBase(id: String, latitud: Double, longitud: Double){
        print("Arrived to firebase")
        let pinRegistro = [
            "idHogar": id,
            "latitud":Double(latitud),
            "longitud":Double(longitud),
            "registrado": false
            ] as [String : Any]
        print("Created json")
        self.refRegistros?.child("mapas").child(actualMap).child("hogares").child((id)).setValue(pinRegistro)
        print("Sended to firebase")
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        let latesLocation: CLLocation
        latesLocation = locations[locations.count - 1]
        initialLocation = CLLocation(latitude: latesLocation.coordinate.latitude, longitude: latesLocation.coordinate.longitude)
        if !firstLoad{
        centerMapOnLocation(location: initialLocation)
        firstLoad = true
        }
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate,
                                                  latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "pin")
        annotationView.canShowCallout = true
        var imageName = ""
        if annotation.subtitle!!.contains("No"){
            imageName = "redPin.png"
        } else {
            imageName = "greenPin.png"
        }
        let pinImage = UIImage(named: imageName)
        let size = CGSize(width: 50, height: 50)
        UIGraphicsBeginImageContext(size)
        pinImage!.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
        
        annotationView.image = resizedImage
        
        let rightButton: AnyObject! = UIButton(type: UIButton.ButtonType.detailDisclosure)
        annotationView.rightCalloutAccessoryView = rightButton as? UIView
        return annotationView
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if newString.count > 15 {
            return false
        }
        return true
    }

}

extension MapPinsViewController: NetworkStatusListener {
    
    func networkStatusDidChange(status: Reachability.NetworkStatus) {
        
        switch status {
        case .notReachable:
            self.generateWarningLabel()
        case .reachableViaWiFi:
            self.deleteWarningLabel()
        case .reachableViaWWAN:
            self.deleteWarningLabel()
        }
        
    }
}
