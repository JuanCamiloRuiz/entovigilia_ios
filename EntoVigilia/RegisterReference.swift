//
//  RegisterReference.swift
//  EntoVigilia
//
//  Created by Juan Camilo Ruiz on 3/18/19.
//  Copyright © 2019 Juan Camilo Ruiz. All rights reserved.
//

import Foundation

class RegisterReference: Codable{
    
    enum CodingKeys: String, CodingKey {
        case latitud
        case longitud
        case departamento
        case municipio
        case vereda
        case id
        case date
        case diaSemana
    }
    
    var latitud: String
    var longitud: String
    var departamento: String
    var municipio: String
    var vereda: String
    var date: String
    var id: String
    var diaSemana: String
    
    init(latitud: String, longitud: String, departamento: String, municipio: String, vereda: String, id: String) {
        self.latitud = latitud
        self.longitud = longitud
        self.departamento = departamento
        self.municipio = municipio
        self.vereda = vereda
        self.id = id
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/LLLL/yyyy HH:mm"
        let str = formatter.string(from: Date())
        
        self.date = str
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EE"
        let dayInWeek = dateFormatter.string(from: Date())
        diaSemana = String(dayInWeek)
    }
    
}
