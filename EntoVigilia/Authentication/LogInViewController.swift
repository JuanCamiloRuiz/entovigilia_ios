//
//  LogInViewController.swift
//  EntoVigilia
//
//  Created by Juan Camilo Ruiz on 4/22/19.
//  Copyright © 2019 Juan Camilo Ruiz. All rights reserved.
//

import UIKit
import Firebase
import ReachabilitySwift

class LogInViewController: UIViewController, UITextFieldDelegate{
    
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var ingresarButton: UIButton!
    @IBOutlet weak var registrarButton: UIButton!
    let reachability = Reachability()!
    let imageView = UIImageView(image: UIImage(named: "noWifi.png"))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        userName.delegate = self
        password.delegate = self
        
        userName!.tag = 0
        password!.tag = 1
        
        updateButtons()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func ingresarTapped(_ sender: UIButton) {
        
        if ingresarButton.titleLabel?.text == "Iniciar" {
        guard let email = self.userName.text, !email.isEmpty else {
            displayMyAlertMessage (userMessage: "Olvidaste ingresar el email para ingresar a tu cuenta")
            return
        }
        
        guard let password = self.password.text, !password.isEmpty else {
            displayMyAlertMessage (userMessage: "Olvidaste ingresar la contraseña para ingresar a tu cuenta")
            return
        }
        
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            
            if let error = error {
                print("Failed to sign user in with error: ", error.localizedDescription)
                self.displayMyAlertMessage (userMessage: error.localizedDescription)
                return
            }
            self.displayCreatedMessage(email: email)
            }
        }
        else {
            do {
            try Auth.auth().signOut()
                self.sendToHome()
            } catch let error {
                print("Failed to sign out with error..", error)
            }
            
        }
    }
    
    func updateButtons(){
        if Auth.auth().currentUser != nil {
            let user = Auth.auth().currentUser
            ingresarButton.setTitle("Cerrar Sesión",for: .normal)
            ingresarButton.tintColor = .red
            ingresarButton.borderColor = .red
            registrarButton.isEnabled = false
            userName.text = user?.email
            userName.isEnabled = false
            password.isEnabled = false
        } else {
            ingresarButton.setTitle("Iniciar",for: .normal)
            ingresarButton.tintColor = UIColor(red: 11/255, green: 201/255, blue: 8/255, alpha: 1.0)
            ingresarButton.borderColor = UIColor(red: 11/255, green: 201/255, blue: 8/255, alpha: 1.0)
            registrarButton.isEnabled = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        ReachabilityManager.shared.addListener(listener: self)
        if reachability.currentReachabilityStatus == Reachability.NetworkStatus.reachableViaWiFi {
            deleteWarningLabel()
        } else if reachability.currentReachabilityStatus == Reachability.NetworkStatus.reachableViaWWAN {
            deleteWarningLabel()
        } else {
            generateWarningLabel()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        ReachabilityManager.shared.removeListener(listener: self)
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1
        
        if let nextResponder = textField.superview?.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    
    func displayMyAlertMessage(userMessage:String){
        var finalMessage = userMessage
        if userMessage.contains("badly formatted"){
           finalMessage = "El email ingresado no posee un formato correcto"
        } else if userMessage.contains("no user record corresponding"){
            finalMessage = "No existe un usuario con el email ingresado"
        } else if userMessage.contains("password is invalid"){
            finalMessage = "La contraseña ingresada no corresponde al usuario con el email ingresado"
        } else if userMessage.contains("Network error"){
            finalMessage = "No tienes conexión a internet, verifica tu conexión y vuelve a intentarlo"
        }
        let myAlert = UIAlertController(title: "Imposible ingresar a tu cuenta", message: finalMessage, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil)
        myAlert.addAction(okAction)
        self.present(myAlert, animated: true, completion: nil)
    }
    
    func displayCreatedMessage(email: String){
        var userMessage: String = " Se ha ingresado a la cuenta de " + email + ""
        var titleMessage: String = "Se ha ingresado correctamente"
        if Auth.auth().currentUser != nil{
            if !Auth.auth().currentUser!.isEmailVerified{
                userMessage = "No has verificado tu email, por tanto no se podrán envíar registros ni consultar o crear mapas"
                titleMessage = "Se ha ingresado pero el email aún no ha sido verificado"
            }
        }
        let myAlert = UIAlertController(title: titleMessage, message: userMessage, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {_ in
            self.sendToHome()
        })
        myAlert.addAction(okAction)
        self.present(myAlert, animated: true, completion: nil)
    }
    
    func sendToHome(){
        performSegue(withIdentifier: "GoHomeView", sender: self)
    }
    
    func displaySignOutMessage(){
        let userMessage: String = "Se ha cerrado tu sesión"
        let myAlert = UIAlertController(title: "Sesión cerrada correctamente", message: userMessage, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {_ in
            let VC = self.storyboard?.instantiateViewController(withIdentifier: "homeNav") as! UINavigationController
            self.present(VC, animated: true, completion: nil)
        })
        myAlert.addAction(okAction)
        self.present(myAlert, animated: true, completion: nil)
    }
    
    func generateWarningLabel(){
        imageView.frame = CGRect(x: 335, y: 65, width: 30, height: 30)
        view.addSubview(imageView)
    }
    
    func deleteWarningLabel(){
        globalHomeVC!.emptyRegisters()
        imageView.removeFromSuperview()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if newString.count > 25 {
            return false
        }
        return true
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension LogInViewController: NetworkStatusListener {
    
    func networkStatusDidChange(status: Reachability.NetworkStatus) {
        
        switch status {
        case .notReachable:
            self.generateWarningLabel()
        case .reachableViaWiFi:
            self.deleteWarningLabel()
        case .reachableViaWWAN:
            self.deleteWarningLabel()
        }
        
    }
}
