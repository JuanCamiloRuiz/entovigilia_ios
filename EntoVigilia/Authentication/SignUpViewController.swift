//
//  SignUpViewController.swift
//  EntoVigilia
//
//  Created by Juan Camilo Ruiz on 4/22/19.
//  Copyright © 2019 Juan Camilo Ruiz. All rights reserved.
//

import UIKit
import Firebase
import ReachabilitySwift

class SignUpViewController: UIViewController,  UITextFieldDelegate{

    
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var password: UITextField!
    let reachability = Reachability()!
    let imageView = UIImageView(image: UIImage(named: "noWifi.png"))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        userName.delegate = self
        password.delegate = self
        
        userName!.tag = 0
        password!.tag = 1
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        ReachabilityManager.shared.addListener(listener: self)
        if reachability.currentReachabilityStatus == Reachability.NetworkStatus.reachableViaWiFi {
            deleteWarningLabel()
        } else if reachability.currentReachabilityStatus == Reachability.NetworkStatus.reachableViaWWAN {
            deleteWarningLabel()
        } else {
            generateWarningLabel()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        ReachabilityManager.shared.removeListener(listener: self)
    }
    
    @IBAction func registrarseTapped(_ sender: UIButton) {
        
        guard let email = self.userName.text, !email.isEmpty else {
            displayMyAlertMessage (userMessage: "Olvidaste ingresar el email para crear tu cuenta")
            return
        }
        
        guard let password = self.password.text, !password.isEmpty else {
            displayMyAlertMessage (userMessage: "Olvidaste ingresar la contraseña para crear tu cuenta")
            return 
        }
        
        Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
            if let error = error {
                print("Failed to sign user up with error: ", error.localizedDescription)
                self.displayMyAlertMessage (userMessage: error.localizedDescription)
                return
            }
            self.displayCreatedMessage(email: email)
            print("User was created succesufly")
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1
        
        if let nextResponder = textField.superview?.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    
    func displayMyAlertMessage(userMessage:String){
        var finalMessage = userMessage
        if userMessage.contains("badly formatted"){
            finalMessage = "El email ingresado no posee un formato correcto"
        } else if userMessage.contains("password must be"){
            finalMessage = "La contraseña debe tener al menos 6 caracteres o más"
        } else if userMessage.contains("Network error"){
            finalMessage = "No tienes conexión a internet, verifica tu conexión y vuelve a intentarlo"
        }
        let myAlert = UIAlertController(title: "Imposible crear el usuario", message: finalMessage, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil)
        myAlert.addAction(okAction)
        self.present(myAlert, animated: true, completion: nil)
    }
    
    func sendToHome(){
        performSegue(withIdentifier: "GoHomeView", sender: self)
    }
    
    func displayCreatedMessage(email: String){
        Auth.auth().currentUser?.sendEmailVerification { (error) in
            if error != nil{
                print("HUBO UN ERROOOOR")
                print(error)
            } else {
                print("email enviado exitosamente")
            }
        }
        let userMessage: String = " Se ha creado el usuario con el email " + email + ", sin embargo no ha sido verificado y por tanto no podrá envíar registros ni consultar o crear mapas"
        let myAlert = UIAlertController(title: "Usuario creado", message: userMessage, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {_ in
            self.sendToHome()
        })
        myAlert.addAction(okAction)
        self.present(myAlert, animated: true, completion: nil)
    }
    
    func displayEmailNotVerifydMessage(email: String){
        let userMessage: String = " El email " + email + " no ha sido verificado, por tanto no se pueden envíar registros ni consultas mapas"
        let myAlert = UIAlertController(title: "Email no verificado", message: userMessage, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {_ in
            self.sendToHome()
        })
        myAlert.addAction(okAction)
        self.present(myAlert, animated: true, completion: nil)
    }
    
    func generateWarningLabel(){
        imageView.frame = CGRect(x: 335, y: 65, width: 30, height: 30)
        view.addSubview(imageView)
    }
    
    func deleteWarningLabel(){
        globalHomeVC!.emptyRegisters()
        imageView.removeFromSuperview()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if newString.count > 25 {
            return false
        }
        return true
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension SignUpViewController: NetworkStatusListener {
    
    func networkStatusDidChange(status: Reachability.NetworkStatus) {
        
        switch status {
        case .notReachable:
            self.generateWarningLabel()
        case .reachableViaWiFi:
            self.deleteWarningLabel()
        case .reachableViaWWAN:
            self.deleteWarningLabel()
        }
        
    }
}
