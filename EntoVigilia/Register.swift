//
//  Register.swift
//  EntoVigilia
//
//  Created by Juan Camilo Ruiz on 3/18/19.
//  Copyright © 2019 Juan Camilo Ruiz. All rights reserved.
//

import Foundation
import os.log

class Register: Codable {
    
    enum CodingKeys: String, CodingKey {
        case peroRegister
        case interRegister
        case refRegister
    }
    
    var peroRegister: RegisterPero
    var interRegister: RegisterInter
    var refRegister: RegisterReference
    
    //MARK: Archiving Paths
    
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("registers")
    static let ToSendArchiveURL = DocumentsDirectory.appendingPathComponent("toSendRegisters")
    
    init(peroRegister: RegisterPero, interRegister: RegisterInter, refRegister: RegisterReference) {
        self.peroRegister = peroRegister
        self.interRegister = interRegister
        self.refRegister = refRegister
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(peroRegister, forKey: .peroRegister)
        try container.encode(interRegister, forKey: .interRegister)
        try container.encode(refRegister, forKey: .refRegister)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        peroRegister = try container.decode(RegisterPero.self, forKey: .peroRegister)
        interRegister = try container.decode(RegisterInter.self, forKey: .interRegister)
        refRegister = try container.decode(RegisterReference.self, forKey: .refRegister)
    }
    
    /*
    func encode(with aCoder: NSCoder) {
        aCoder.encode(peroRegister, forKey: PropertyKey.perodom)
        aCoder.encode(interRegister, forKey: PropertyKey.interdom)
        aCoder.encode(refRegister, forKey: PropertyKey.reference)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
    
        guard let peroReg = aDecoder.decodeObject(forKey: PropertyKey.perodom) as? RegisterPero else {
            os_log("Unable to decode the peroRegister for a Register object.", log: OSLog.default, type: .debug)
            return nil
        }
        guard let interReg = aDecoder.decodeObject(forKey: PropertyKey.interdom) as? RegisterInter else {
            os_log("Unable to decode the interRegister for a Register object.", log: OSLog.default, type: .debug)
            return nil
        }
        guard let refReg = aDecoder.decodeObject(forKey: PropertyKey.reference) as? RegisterReference else {
            os_log("Unable to decode the interRegister for a Register object.", log: OSLog.default, type: .debug)
            return nil
        }
        self.init(peroRegister: peroReg, interRegister: interReg, refRegister: refReg)
    }
 */

}
