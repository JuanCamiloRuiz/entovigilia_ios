//
//  RegisterTableView.swift
//  EntoVigilia
//
//  Created by Juan Camilo Ruiz on 3/19/19.
//  Copyright © 2019 Juan Camilo Ruiz. All rights reserved.
//

import UIKit

class RegisterTableView: UITableView {

    //MARK: properties
    
    var registers = [Register]()
    
    private func loadRegisters(){
        let regs = retrieveRegisters()
        for reg in regs!{
            registers.append(reg)
        }
        
    }
    
    func retrieveRegisters() -> [Register]? {
        guard let data = NSKeyedUnarchiver.unarchiveObject(withFile: Register.ArchiveURL.path) as? Data else { return nil }
        do {
            let registers = try PropertyListDecoder().decode([Register].self, from: data)
            return registers
        } catch {
            print("Retrieve Failed")
            return nil
        }
    }

}
