//
//  pinRegister.swift
//  EntoVigilia
//
//  Created by Juan Camilo Ruiz on 4/3/19.
//  Copyright © 2019 Juan Camilo Ruiz. All rights reserved.
//

import Foundation

class PinRegisterSimplify {
    
    var id: String
    var latitud: Double
    var longitud: Double
    
    init(id: String, latitud: Double, longitud: Double) {
        self.id = id
        self.latitud = latitud
        self.longitud = longitud
    }
}
