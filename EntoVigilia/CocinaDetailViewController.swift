//
//  CocinaDetailViewController.swift
//  EntoVigilia
//
//  Created by Juan Camilo Ruiz on 3/23/19.
//  Copyright © 2019 Juan Camilo Ruiz. All rights reserved.
//

import UIKit
import AVFoundation
import ReachabilitySwift

class CocinaDetailViewController: UIViewController, AVAudioPlayerDelegate{

    let reachability = Reachability()!
    var actualRegister: Register?
    var titleRegister: String?
    var audioPlayer:AVAudioPlayer!
    var audRegister: String?
    let imageView = UIImageView(image: UIImage(named: "noWifi.png"))
    
    @IBOutlet weak var rastroField: UITextField!
    @IBOutlet weak var huevosField: UITextField!
    @IBOutlet weak var larvasField: UITextField!
    @IBOutlet weak var ninfasField: UITextField!
    @IBOutlet weak var adultosField: UITextField!
    @IBOutlet weak var imageCocina: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var playButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadRegister()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        ReachabilityManager.shared.addListener(listener: self)
        if reachability.currentReachabilityStatus == Reachability.NetworkStatus.reachableViaWiFi {
            deleteWarningLabel()
        } else if reachability.currentReachabilityStatus == Reachability.NetworkStatus.reachableViaWWAN {
            deleteWarningLabel()
        } else {
            generateWarningLabel()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        ReachabilityManager.shared.removeListener(listener: self)
    }
    
    @IBAction func imageTapped(_ sender: UITapGestureRecognizer) {
        print("Image was tapped")
        let imageView = sender.view as! UIImageView
        let newImageView = UIImageView(image: imageView.image)
        newImageView.frame = UIScreen.main.bounds
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        newImageView.addGestureRecognizer(tap)
        self.view.addSubview(newImageView)
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = true
    }
    
    @IBAction func playButtonTapped(_ sender: UIButton) {
        if (sender.titleLabel?.text == "Reproducir"){
            sender.setTitle("Detener", for: .normal)
            sender.setImage(UIImage(named: "pause.png"), for: .normal)
            preparePlayer()
            audioPlayer.play()
        } else {
            audioPlayer.stop()
            sender.setTitle("Reproducir", for: .normal)
            sender.setImage(UIImage(named: "play.png"), for: .normal)
        }
    }
    
    func preparePlayer() {
        var error: NSError?
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: getDocumentsDirectory().appendingPathComponent(audRegister!) as URL)
        } catch let error1 as NSError {
            error = error1
            audioPlayer = nil
        }
        
        if let err = error {
            print("AVAudioPlayer error: \(err.localizedDescription)")
        } else {
            audioPlayer.delegate = self
            audioPlayer.prepareToPlay()
            audioPlayer.volume = 10.0
        }
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        self.navigationController?.isNavigationBarHidden = false
        self.tabBarController?.tabBar.isHidden = false
        sender.view?.removeFromSuperview()
    }
    
    private func loadRegister(){
        let registerId = actualRegister!.refRegister.id
        let document = try!
            FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        titleLabel.text = titleRegister!
        
        if titleRegister! == "Cocina" {
        var interRegister: RegisterInter
        interRegister = actualRegister!.interRegister
        rastroField.text = interRegister.rastroCocinaValue
        huevosField.text = interRegister.huevosCocinaValue
        larvasField.text = interRegister.larvasCocinaValue
        ninfasField.text = interRegister.ninfasCocinaValue
        adultosField.text = interRegister.adultosCocinaValue
        audRegister = "cocinaAudio" + registerId + ".m4a"
        let nameImgCocina = "imgCocina" + registerId + ".png"
        let imgCocinaUrl = document.appendingPathComponent(nameImgCocina, isDirectory: true)
        imageCocina.image = UIImage(contentsOfFile: imgCocinaUrl.path)
        }
        else if titleRegister! == "Dormitorio" {
            var interRegister: RegisterInter
            interRegister = actualRegister!.interRegister
            rastroField.text = interRegister.rastroDormitorioValue
            huevosField.text = interRegister.huevosDormitorioValue
            larvasField.text = interRegister.larvasDormitorioValue
            ninfasField.text = interRegister.ninfasDormitorioValue
            adultosField.text = interRegister.adultosDormitorioValue
            audRegister = "dormAudio" + registerId + ".m4a"
            let nameImgCocina = "imgDormitorio" + registerId + ".png"
            let imgCocinaUrl = document.appendingPathComponent(nameImgCocina, isDirectory: true)
            imageCocina.image = UIImage(contentsOfFile: imgCocinaUrl.path)
        } else if titleRegister! == "ZonaSocial" {
            var interRegister: RegisterInter
            interRegister = actualRegister!.interRegister
            rastroField.text = interRegister.rastroZonaSocialValue
            huevosField.text = interRegister.huevosZonaSocialValue
            larvasField.text = interRegister.larvasZonaSocialValue
            ninfasField.text = interRegister.ninfasZonaSocialValue
            adultosField.text = interRegister.adultosZonaSocialValue
            audRegister = "zonaAudio" + registerId + ".m4a"
            let nameImgCocina = "imgZonaSocial" + registerId + ".png"
            let imgCocinaUrl = document.appendingPathComponent(nameImgCocina, isDirectory: true)
            imageCocina.image = UIImage(contentsOfFile: imgCocinaUrl.path)
        } else if titleRegister! == "Gallinero" {
            var periRegistro: RegisterPero
            periRegistro = actualRegister!.peroRegister
            rastroField.text = periRegistro.rastroGallineroValue
            huevosField.text = periRegistro.huevosGallineroValue
            larvasField.text = periRegistro.larvasGallineroValue
            ninfasField.text = periRegistro.ninfasGallineroValue
            adultosField.text = periRegistro.adultosGallineroValue
            audRegister = "gallAudio" + registerId + ".m4a"
            let nameImgCocina = "imgGallinero" + registerId + ".png"
            let imgCocinaUrl = document.appendingPathComponent(nameImgCocina, isDirectory: true)
            imageCocina.image = UIImage(contentsOfFile: imgCocinaUrl.path)
        }else if titleRegister! == "Corral" {
            var periRegistro: RegisterPero
            periRegistro = actualRegister!.peroRegister
            rastroField.text = periRegistro.rastroCorralValue
            huevosField.text = periRegistro.huevosCorralValue
            larvasField.text = periRegistro.larvasCorralValue
            ninfasField.text = periRegistro.ninfasCorralValue
            adultosField.text = periRegistro.adultosCorralValue
            audRegister = "corrAudio" + registerId + ".m4a"
            let nameImgCocina = "imgCorral" + registerId + ".png"
            let imgCocinaUrl = document.appendingPathComponent(nameImgCocina, isDirectory: true)
            imageCocina.image = UIImage(contentsOfFile: imgCocinaUrl.path)
        }else if titleRegister! == "Deposito" {
            var periRegistro: RegisterPero
            periRegistro = actualRegister!.peroRegister
            rastroField.text = periRegistro.rastroDepositoValue
            huevosField.text = periRegistro.huevosDepositoValue
            larvasField.text = periRegistro.larvasDepositoValue
            ninfasField.text = periRegistro.ninfasDepositoValue
            adultosField.text = periRegistro.adultosDepositoValue
            audRegister = "depoAudio" + registerId + ".m4a"
            let nameImgCocina = "imgDeposito" + registerId + ".png"
            let imgCocinaUrl = document.appendingPathComponent(nameImgCocina, isDirectory: true)
            imageCocina.image = UIImage(contentsOfFile: imgCocinaUrl.path)
        }
        else {
            print("Error, no se sabe que tipo de registro se esta mostrando")
        }
        enablePlayButton()
    }
    
    func enablePlayButton(){
        let filemanager = FileManager.default
        if  filemanager.fileExists(atPath: getDocumentsDirectory().appendingPathComponent(audRegister!).path){
           playButton.isEnabled = true
        } else {
           playButton.isEnabled = false
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailButton" {
            let detailVC: RegisterDetailViewController = segue.destination as! RegisterDetailViewController
            detailVC.actualRegister = actualRegister
        }
    }

    func generateWarningLabel(){
        imageView.frame = CGRect(x: 335, y: 65, width: 30, height: 30)
        view.addSubview(imageView)
    }
    
    func deleteWarningLabel(){
        imageView.removeFromSuperview()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension CocinaDetailViewController: NetworkStatusListener {
    
    func networkStatusDidChange(status: Reachability.NetworkStatus) {
        
        switch status {
        case .notReachable:
            self.generateWarningLabel()
        case .reachableViaWiFi:
            self.deleteWarningLabel()
        case .reachableViaWWAN:
            self.deleteWarningLabel()
        }
        
    }
}
