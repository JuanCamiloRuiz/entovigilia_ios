//
//  ShowCreateOptionsViewController.swift
//  EntoVigilia
//
//  Created by Juan Camilo Ruiz on 4/3/19.
//  Copyright © 2019 Juan Camilo Ruiz. All rights reserved.
//

import UIKit

class ShowCreateOptionsViewController: UIViewController {

    
    @IBOutlet weak var createView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        makeRounded()
        self.showAnimate()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func close(_ sender: UIButton) {
        self.removeAnimate() 
    }
    
    func makeRounded() {
        createView.layer.cornerRadius = 5
        createView.layer.shadowColor = UIColor.black.cgColor
        createView.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        createView.layer.masksToBounds = false
        createView.layer.shadowRadius = 2.0
        createView.layer.shadowOpacity = 0.5
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
