//
//  HomeViewController.swift
//  EntoVigilia
//
//  Created by Juan Camilo Ruiz on 3/19/19.
//  Copyright © 2019 Juan Camilo Ruiz. All rights reserved.
//

import UIKit
import ReachabilitySwift
import Firebase

var globalHomeVC: HomeViewController?

class HomeViewController: UIViewController {

    let reachability = Reachability()!
    var theresConection = true
    var messageShowed = false
    var noUserMessageShowed = false
    let imageView = UIImageView(image: UIImage(named: "noWifi.png"))
    
    @IBOutlet weak var createButton: UIButton!
    @IBOutlet weak var usuarioRegistrado: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        globalHomeVC = self
        if !theresConection && !messageShowed{
            showNoConexionMessage()
        }
        
        if checkToSendRegister() && !noUserMessageShowed && Auth.auth().currentUser == nil{
            showNoUserMessage()
        }
        
        resetValues()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector (tap))  //Tap function will call when user tap on button
        let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(createRegister))  //Long function will call when user long press on button.
        tapGesture.numberOfTapsRequired = 1
        createButton.addGestureRecognizer(tapGesture)
        createButton.addGestureRecognizer(longGesture)
        
        loadUser()
    }
    
    func checkToSendRegister() -> Bool{
        guard let data = NSKeyedUnarchiver.unarchiveObject(withFile: Register.ToSendArchiveURL.path) as? Data else { return false}
        do {
            let registers = try PropertyListDecoder().decode([Register].self, from: data)
            if registers.count > 0{
                return true }
            else {
                return false }
        } catch {
            print("Retrieve Failed")
            return false
        }
        return false
    }
    
    func showNoUserMessage(){
        let alert = UIAlertController(title: "No estas como usuario", message: "Los registros se guardaran localmente hasta que ingreses a una cuenta de usuario y se puedan subir a la base de datos", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
        noUserMessageShowed = true
    }
    
    func loadUser(){
        if Auth.auth().currentUser != nil {
            let user = Auth.auth().currentUser
            if user!.isEmailVerified{
                usuarioRegistrado.text = "Usuario: " + (user?.email!)! + " (verificado)"
                sendRegisters()
            } else {
                usuarioRegistrado.text = "Usuario: " + (user?.email!)! + " (no verificado)"
            }
            usuarioRegistrado.isHidden = false
        } else {
            usuarioRegistrado.isHidden = true
        }
    }
    
    func sendRegisters(){
        guard let data = NSKeyedUnarchiver.unarchiveObject(withFile: Register.ToSendArchiveURL.path) as? Data else { return }
        do {
            let registers = try PropertyListDecoder().decode([Register].self, from: data)
            for reg in registers{
                globalRegisterReferenceVC?.sendFireBase(register: reg)
            }
        } catch {
            print("Retrieve Failed")
            return
        }
        let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
        do {
            try FileManager.default.removeItem(at: DocumentsDirectory.appendingPathComponent("toSendRegisters"))
        }
        catch{
            print("There were no to send registers")
        }
        noUserMessageShowed = false
    }
    
    @objc func tap(){
        performSegue(withIdentifier: "tabBarSegue", sender: createButton)
    }
    
    func resetValues(){
        /*
        if globalRegisterInterVC != nil {
            print("Setting inter to default values")
            globalRegisterInterVC?.setDefaults()
        }*/
        
        UserDefaults.standard.removeObject(forKey: "cocinaValues")
        UserDefaults.standard.removeObject(forKey: "dormitorioValues")
        UserDefaults.standard.removeObject(forKey: "zonaValues")
        UserDefaults.standard.removeObject(forKey: "gallineroValues")
        UserDefaults.standard.removeObject(forKey: "corralValues")
        UserDefaults.standard.removeObject(forKey: "depositoValues")
        
        if globalCorralVC != nil {
           globalCorralVC?.resetValues()
        }
        if globalCocinaVC != nil {
           globalCocinaVC?.resetValues()
        }
        if globalGallineroVC != nil {
           globalGallineroVC?.resetValues()
        }
        if globalDepositoVC != nil {
            globalDepositoVC?.resetValues()
        }
        if globalZonaVC != nil {
           globalZonaVC?.resetValues()
        }
        if globalDormitorioVC != nil {
           globalDormitorioVC?.resetValues()
        }
        
        let document = try!
            FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        do {
            try FileManager.default.removeItem(at: document.appendingPathComponent("imgCocina.png", isDirectory: true))
        }
        catch{
        print("There was not an cocina image")
        }
        do {
            try FileManager.default.removeItem(at: document.appendingPathComponent("imgDormitorio.png", isDirectory: true))
        }
        catch{
            print("There was not an imgDormitorio.png image")
        }
        do {
            try FileManager.default.removeItem(at: document.appendingPathComponent("imgZonaSocial.png", isDirectory: true))
        }
        catch{
            print("There was not an imgZonaSocial.png image")
        }
        do {
            try FileManager.default.removeItem(at: document.appendingPathComponent("imgGallinero.png", isDirectory: true))
        }
        catch{
            print("There was not an imgGallinero.png image")
        }
        do {
            try FileManager.default.removeItem(at: document.appendingPathComponent("imgCorral.png", isDirectory: true))
        }
        catch{
            print("There was not an imgCorral.png image")
        }
        do {
            try FileManager.default.removeItem(at: document.appendingPathComponent("imgDeposito.png", isDirectory: true))
        }
        catch{
            print("There was not an imgDeposito.png image")
        }
        let audiosValues = ["cocinaAudio.m4a","dormAudio.m4a", "zonaAudio.m4a", "gallAudio.m4a", "corrAudio.m4a", "depoAudio.m4a"]
        for i in 0...(audiosValues.count-1){
            do {
                try FileManager.default.removeItem(at: document.appendingPathComponent(audiosValues[i], isDirectory: true))
            }
            catch{
                print("There was not an " + audiosValues[i])
            }
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @objc func createRegister(_ sender: UIGestureRecognizer) {
        if sender.state == .ended {
            print("UIGestureRecognizerStateEnded")
            //Do Whatever You want on End of Gesture
        if globalRegisterPeroVC != nil{
           globalRegisterPeroVC?.viewDidLoad()
        }
        if globalRegisterInterVC != nil {
            globalRegisterInterVC?.viewDidLoad()
        }
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "showCreateOptions") as! ShowCreateOptionsViewController
        self.addChild(popOverVC)
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParent: self)
        }
        else if sender.state == .began {
            print("UIGestureRecognizerStateBegan.")
            //Do Whatever You want on Began of Gesture
        }
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        ReachabilityManager.shared.addListener(listener: self)
        if reachability.currentReachabilityStatus == Reachability.NetworkStatus.reachableViaWiFi {
            theresConection = true
            deleteWarningLabel()
        } else if reachability.currentReachabilityStatus == Reachability.NetworkStatus.reachableViaWWAN {
            theresConection = true
            deleteWarningLabel()
        } else {
            theresConection = false
            generateWarningLabel()
        }
        if !theresConection && !messageShowed{
            showNoConexionMessage()
        }
        if checkToSendRegister() && !noUserMessageShowed && Auth.auth().currentUser == nil{
            showNoUserMessage()
        }
        resetValues()
        loadUser()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        ReachabilityManager.shared.removeListener(listener: self)
    }

    func generateWarningLabel(){
        imageView.frame = CGRect(x: 335, y: 65, width: 30, height: 30)
        view.addSubview(imageView)
    }
    
    func deleteWarningLabel(){
        emptyRegisters()
        imageView.removeFromSuperview()
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    func emptyRegisters(){
        var newRegisters: [Register] = []
        var registersSaved: [Register]? = retrieveRegisters()
        let document = try!
            FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        if registersSaved != nil{
        for reg in registersSaved! {
            var id = reg.refRegister.id
            var hasImage = false
            let nameImgCocina = "imgCocina" + id + ".png"
            let imgCocinaUrl = document.appendingPathComponent(nameImgCocina, isDirectory: true)
            let nameImgDorm = "imgDormitorio" + id + ".png"
            let imgDormitorioUrl = document.appendingPathComponent(nameImgDorm, isDirectory: true)
            let nameImgZonaSocial = "imgZonaSocial" + id + ".png"
            let imgZonaSocialUrl = document.appendingPathComponent(nameImgZonaSocial, isDirectory: true)
            let nameImgGallinero = "imgGallinero" + id + ".png"
            let imgGallinerolUrl = document.appendingPathComponent(nameImgGallinero, isDirectory: true)
            let nameImgCorral = "imgCorral" + id + ".png"
            let imgCorralUrl = document.appendingPathComponent(nameImgCorral, isDirectory: true)
            let nameImgDep = "imgDeposito" + id + ".png"
            let imgDepositoUrl = document.appendingPathComponent(nameImgDep, isDirectory: true)
            if UIImage(contentsOfFile: imgCocinaUrl.path) != nil {
                hasImage = true
            } else if UIImage(contentsOfFile: imgDormitorioUrl.path) != nil{
                hasImage = true
            } else if UIImage(contentsOfFile: imgZonaSocialUrl.path) != nil {
                hasImage = true
            } else if UIImage(contentsOfFile: imgGallinerolUrl.path) != nil {
                hasImage = true
            } else if UIImage(contentsOfFile: imgCorralUrl.path) != nil {
                hasImage = true
            } else if UIImage(contentsOfFile: imgDepositoUrl.path) != nil {
                hasImage = true
            }
            if hasImage {
                newRegisters.append(reg)
            }
            }}
        self.saveRegisters(regs: newRegisters)
    }
    
    func showNoConexionMessage(){
        let alert = UIAlertController(title: "No hay conexión", message: "Los registros se guardaran localmente hasta que haya conexión y se puedan subir a la base de datos", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
        messageShowed = true
    }
    
    private func saveRegisters(regs: [Register]) {
        do {
            let data = try PropertyListEncoder().encode(regs)
            let success = NSKeyedArchiver.archiveRootObject(data, toFile: Register.ArchiveURL.path)
            print(success ? "Successful save" : "Save Failed")
        } catch {
            print("Save Failed")
        }
    }
    
    func retrieveRegisters() -> [Register]? {
        guard let data = NSKeyedUnarchiver.unarchiveObject(withFile: Register.ArchiveURL.path) as? Data else { return nil }
        do {
            let registers = try PropertyListDecoder().decode([Register].self, from: data)
            return registers
        } catch {
            print("Retrieve Failed")
            return nil
        }
    }
    
    @IBAction func unwindToHome(_ unwindSegue: UIStoryboardSegue) {
    }
}

extension HomeViewController: NetworkStatusListener {
    
    func networkStatusDidChange(status: Reachability.NetworkStatus) {
        
        switch status {
        case .notReachable:
            theresConection = false
            self.generateWarningLabel()
            if !messageShowed{
            showNoConexionMessage()
            }
        case .reachableViaWiFi:
            theresConection = true
            self.deleteWarningLabel()
        case .reachableViaWWAN:
            theresConection = true
            self.deleteWarningLabel()
        }
         
    }
}

@IBDesignable extension UIButton {
    
    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
}
