//
//  pinRegister.swift
//  EntoVigilia
//
//  Created by Juan Camilo Ruiz on 4/3/19.
//  Copyright © 2019 Juan Camilo Ruiz. All rights reserved.
//

import Foundation

class pinRegister {
    
    var id: String
    var latitud: Double
    var longitud: Double
    var departamento: String
    var municipio: String
    var fecha: String
    
    init(id: String, latitud: Double, longitud: Double, departamento: String, municipio: String, fecha: String) {
        self.id = id
        self.latitud = latitud
        self.longitud = longitud
        self.departamento = departamento
        self.municipio = municipio
        self.fecha = fecha
    }
}
