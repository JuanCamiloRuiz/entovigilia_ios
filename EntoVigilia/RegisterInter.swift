//
//  RegisterInter.swift
//  EntoVigilia
//
//  Created by Juan Camilo Ruiz on 3/18/19.
//  Copyright © 2019 Juan Camilo Ruiz. All rights reserved.
//

import Foundation

class RegisterInter: Codable{
    
    enum CodingKeys: String, CodingKey {
        case rastroCocinaValue
        case huevosCocinaValue
        case larvasCocinaValue
        case ninfasCocinaValue
        case adultosCocinaValue
        case rastroDormitorioValue
        case huevosDormitorioValue
        case larvasDormitorioValue
        case ninfasDormitorioValue
        case adultosDormitorioValue
        case rastroZonaSocialValue
        case huevosZonaSocialValue
        case larvasZonaSocialValue
        case ninfasZonaSocialValue
        case adultosZonaSocialValue
    }
    
    var rastroCocinaValue: String
    var huevosCocinaValue: String
    var larvasCocinaValue: String
    var ninfasCocinaValue: String
    var adultosCocinaValue: String
    var rastroDormitorioValue: String
    var huevosDormitorioValue: String
    var larvasDormitorioValue: String
    var ninfasDormitorioValue: String
    var adultosDormitorioValue: String
    var rastroZonaSocialValue: String
    var huevosZonaSocialValue: String
    var larvasZonaSocialValue: String
    var ninfasZonaSocialValue: String
    var adultosZonaSocialValue: String
    
    init( rastroCocinaValue: String, huevosCocinaValue: String, larvasCocinaValue: String, ninfasCocinaValue: String, adultosCocinaValue: String, rastroDormitorioValue: String, huevosDormitorioValue: String, larvasDormitorioValue: String, ninfasDormitorioValue: String, adultosDormitorioValue: String, rastroZonaSocialValue: String, huevosZonaSocialValue: String, larvasZonaSocialValue: String, ninfasZonaSocialValue: String, adultosZonaSocialValue: String){
        self.rastroCocinaValue = rastroCocinaValue
        self.huevosCocinaValue = huevosCocinaValue
        self.larvasCocinaValue = larvasCocinaValue
        self.ninfasCocinaValue = ninfasCocinaValue
        self.adultosCocinaValue = adultosCocinaValue
        self.rastroDormitorioValue = rastroDormitorioValue
        self.huevosDormitorioValue = huevosDormitorioValue
        self.larvasDormitorioValue = larvasDormitorioValue
        self.ninfasDormitorioValue = ninfasDormitorioValue
        self.adultosDormitorioValue = adultosDormitorioValue
        self.rastroZonaSocialValue = rastroZonaSocialValue
        self.huevosZonaSocialValue = huevosZonaSocialValue
        self.larvasZonaSocialValue = larvasZonaSocialValue
        self.ninfasZonaSocialValue = ninfasZonaSocialValue
        self.adultosZonaSocialValue = adultosZonaSocialValue
    }
    
}
