//
//  RegisterPero.swift
//  EntoVigilia
//
//  Created by Juan Camilo Ruiz on 3/15/19.
//  Copyright © 2019 Juan Camilo Ruiz. All rights reserved.
//

import Foundation
import UIKit

class RegisterPero: Codable{
    
    enum CodingKeys: String, CodingKey {
        case rastroGallineroValue
        case huevosGallineroValue
        case larvasGallineroValue
        case ninfasGallineroValue
        case adultosGallineroValue
        case rastroCorralValue
        case huevosCorralValue
        case larvasCorralValue
        case ninfasCorralValue
        case adultosCorralValue
        case rastroDepositoValue
        case huevosDepositoValue
        case larvasDepositoValue
        case ninfasDepositoValue
        case adultosDepositoValue
    }
    
    var rastroGallineroValue: String
    var huevosGallineroValue: String
    var larvasGallineroValue: String
    var ninfasGallineroValue: String
    var adultosGallineroValue: String
    var rastroCorralValue: String
    var huevosCorralValue: String
    var larvasCorralValue: String
    var ninfasCorralValue: String
    var adultosCorralValue: String
    var rastroDepositoValue: String
    var huevosDepositoValue: String
    var larvasDepositoValue: String
    var ninfasDepositoValue: String
    var adultosDepositoValue: String

    
    init(rastroGallineroValue: String, huevosGallineroValue: String, larvasGallineroValue: String, ninfasGallineroValue: String, adultosGallineroValue: String, rastroCorralValue: String, huevosCorralValue: String, larvasCorralValue: String, ninfasCorralValue: String, adultosCorralValue: String, rastroDepositoValue: String, huevosDepositoValue: String, larvasDepositoValue: String, ninfasDepositoValue: String, adultosDepositoValue: String){
        self.rastroGallineroValue = rastroGallineroValue
        self.huevosGallineroValue = huevosGallineroValue
        self.larvasGallineroValue = larvasGallineroValue
        self.ninfasGallineroValue = ninfasGallineroValue
        self.adultosGallineroValue = adultosGallineroValue
        self.rastroCorralValue = rastroCorralValue
        self.huevosCorralValue = huevosCorralValue
        self.larvasCorralValue = larvasCorralValue
        self.ninfasCorralValue = ninfasCorralValue
        self.adultosCorralValue = adultosCorralValue
        self.rastroDepositoValue = rastroDepositoValue
        self.huevosDepositoValue = huevosDepositoValue
        self.larvasDepositoValue = larvasDepositoValue
        self.ninfasDepositoValue = ninfasDepositoValue
        self.adultosDepositoValue = adultosDepositoValue
    }
    
}
