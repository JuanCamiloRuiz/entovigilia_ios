//
//  RegisterInterViewController.swift
//  EntoVigilia
//
//  Created by Juan Camilo Ruiz on 3/18/19.
//  Copyright © 2019 Juan Camilo Ruiz. All rights reserved.
//

import UIKit
import ReachabilitySwift
import AVFoundation

var globalRegisterInterVC: RegisterInterViewController?

class RegisterInterViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate{

    var interRegisters: [RegisterInter] = []
    private var roundButton = UIButton()
    var imagePickerController = UIImagePickerController()
    var imagePicked = 0
    let imageView = UIImageView(image: UIImage(named: "noWifi.png"))
    let reachability = Reachability()!
    private var roundFlashButton = UIButton()
    
    @IBOutlet weak var cocinaVectoresLabel: UILabel!
    @IBOutlet weak var dormitorioVectoresLabel: UILabel!
    @IBOutlet weak var zonaSocialVectoresLabel: UILabel!
    @IBOutlet weak var cocinaFotoAgregada: UIImageView!
    @IBOutlet weak var dormitorioFotoAgregada: UIImageView!
    @IBOutlet weak var zonaFotoAgregada: UIImageView!
    @IBOutlet weak var cocinaAudioExist: UIImageView!
    @IBOutlet weak var dormitorioAudioExist: UIImageView!
    @IBOutlet weak var zonaAudioExist: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        globalRegisterInterVC = self
        
        createFlashButton()
    }
    
    func createFlashButton() {
        roundFlashButton = UIButton(type: .custom)
        roundFlashButton.translatesAutoresizingMaskIntoConstraints = false
        // roundFlashButton.backgroundColor = .green
        // Make sure you replace the name of the image:
        roundFlashButton.setImage(UIImage(named:"flashlight"), for: .normal)
        // Make sure to create a function and replace DOTHISONTAP with your own function:
        roundFlashButton.addTarget(self, action: #selector(flashLigthTapped), for: UIControl.Event.touchUpInside)
        // We're manipulating the UI, must be on the main thread:
        DispatchQueue.main.async {
            if let keyWindow = UIApplication.shared.keyWindow {
                keyWindow.addSubview(self.roundFlashButton)
                NSLayoutConstraint.activate([
                    keyWindow.trailingAnchor.constraint(equalTo: self.roundFlashButton.trailingAnchor, constant: 15),
                    keyWindow.bottomAnchor.constraint(equalTo: self.roundFlashButton.bottomAnchor, constant: 600),
                    self.roundFlashButton.widthAnchor.constraint(equalToConstant: 45),
                    self.roundFlashButton.heightAnchor.constraint(equalToConstant: 45)])
            }
            // Make the button round:
            self.roundFlashButton.layer.cornerRadius = 26
        }
    }
    
    @objc func flashLigthTapped() {
        guard let device = AVCaptureDevice.default(for: AVMediaType.video) else { return }
        guard device.hasTorch else { return }
        
        do {
            try device.lockForConfiguration()
            
            if (device.torchMode == AVCaptureDevice.TorchMode.on) {
                device.torchMode = AVCaptureDevice.TorchMode.off
            } else {
                do {
                    try device.setTorchModeOn(level: 1.0)
                } catch {
                    print(error)
                }
            }
            
            device.unlockForConfiguration()
        } catch {
            print(error)
        }
    }
    
    func saveImages(id: String){
        if globalCocinaVC != nil {
            globalCocinaVC?.saveImages(id: id)
        }
        if globalDormitorioVC != nil {
            globalDormitorioVC?.saveImages(id: id)
        }
        if globalZonaVC != nil {
            globalZonaVC?.saveImages(id: id)
        }
    }
    
    func saveAudios(id: String){
        if globalCocinaVC != nil {
            globalCocinaVC?.saveAudios(id: id)
        }
        if globalDormitorioVC != nil {
            globalDormitorioVC?.saveAudios(id: id)
        }
        if globalZonaVC != nil {
            globalZonaVC?.saveAudios(id: id)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.roundButton.isEnabled = true
        self.roundButton.isHidden = false
        createFloatingButton()
        createFlashButton()
        ReachabilityManager.shared.addListener(listener: self)
        if reachability.currentReachabilityStatus == Reachability.NetworkStatus.reachableViaWiFi {
            deleteWarningLabel()
        } else if reachability.currentReachabilityStatus == Reachability.NetworkStatus.reachableViaWWAN {
            deleteWarningLabel()
        } else {
            generateWarningLabel()
        }
        updateStuff() 
    }
    
    func updateStuff(){
        if globalCocinaVC != nil {
            cocinaVectoresLabel.text = String("Vectores totales: ") + (globalCocinaVC?.getTotalVectors())!
            if globalCocinaVC!.ThereIsImage() {
                print("There was an image in cocina")
                cocinaFotoAgregada.image = UIImage(named: "check.png")
            } else {
                print("There was NO image in cocina")
                cocinaFotoAgregada.image = UIImage(named: "wrong.png")
            }
            if globalCocinaVC!.ThereIsAudio() {
                cocinaAudioExist.image = UIImage(named: "check.png")
            } else {
                cocinaAudioExist.image = UIImage(named: "wrong.png")
            }
        }
        if globalDormitorioVC != nil {
            dormitorioVectoresLabel.text = String("Vectores totales: ") + (globalDormitorioVC?.getTotalVectors())!
            if globalDormitorioVC!.ThereIsImage() {
                dormitorioFotoAgregada.image = UIImage(named: "check.png")
            } else {
                print("There was NO image in cocina")
                dormitorioFotoAgregada.image = UIImage(named: "wrong.png")
            }
            if globalDormitorioVC!.ThereIsAudio() {
                dormitorioAudioExist.image = UIImage(named: "check.png")
            } else {
                dormitorioAudioExist.image = UIImage(named: "wrong.png")
            }
        }
        if globalZonaVC != nil {
            zonaSocialVectoresLabel.text = String("Vectores totales: ") + (globalZonaVC?.getTotalVectors())!
            if globalZonaVC!.ThereIsImage() {
                zonaFotoAgregada.image = UIImage(named: "check.png")
            } else {
                print("There was NO image in cocina")
                zonaFotoAgregada.image = UIImage(named: "wrong.png")
            }
            if globalZonaVC!.ThereIsAudio() {
                zonaAudioExist.image = UIImage(named: "check.png")
            } else {
                zonaAudioExist.image = UIImage(named: "wrong.png")
            }
        }
    }
      
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        if roundButton.superview != nil {
            DispatchQueue.main.async {
                self.roundButton.removeFromSuperview()
                self.roundButton.isEnabled = false
                self.roundButton.isHidden = true
            }
        }
        if roundFlashButton.superview != nil {
            DispatchQueue.main.async {
                self.roundFlashButton.removeFromSuperview()
                self.roundFlashButton.isEnabled = false
                self.roundFlashButton.isHidden = true
            }
        }
        ReachabilityManager.shared.removeListener(listener: self)
    }
    
    func createFloatingButton() {
        roundButton = UIButton(type: .custom)
        roundButton.translatesAutoresizingMaskIntoConstraints = false
        roundButton.backgroundColor = UIColor(red: 11/255, green: 201/255, blue: 8/255, alpha: 1.0)
        // Make sure you replace the name of the image:
        roundButton.setImage(UIImage(named:"sendImage"), for: .normal)
        // Make sure to create a function and replace DOTHISONTAP with your own function:
        roundButton.addTarget(self, action: #selector(saveRegister), for: UIControl.Event.touchUpInside)
        // We're manipulating the UI, must be on the main thread:
        DispatchQueue.main.async {
            if let keyWindow = UIApplication.shared.keyWindow {
                keyWindow.addSubview(self.roundButton)
                NSLayoutConstraint.activate([
                    keyWindow.trailingAnchor.constraint(equalTo: self.roundButton.trailingAnchor, constant: 15),
                    keyWindow.bottomAnchor.constraint(equalTo: self.roundButton.bottomAnchor, constant: 50),
                    self.roundButton.widthAnchor.constraint(equalToConstant: 50),
                    self.roundButton.heightAnchor.constraint(equalToConstant: 50)])
            }
            // Make the button round:
            self.roundButton.layer.cornerRadius = 26
            // Add a black shadow:
            self.roundButton.layer.shadowColor = UIColor.black.cgColor
            self.roundButton.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
            self.roundButton.layer.masksToBounds = false
            self.roundButton.layer.shadowRadius = 2.0
            self.roundButton.layer.shadowOpacity = 0.5
            // Add a pulsing animation to draw attention to button:
            let scaleAnimation: CABasicAnimation = CABasicAnimation(keyPath: "transform.scale")
            scaleAnimation.duration = 0.4
            scaleAnimation.repeatCount = .greatestFiniteMagnitude
            scaleAnimation.autoreverses = true
            scaleAnimation.fromValue = 1.0;
            scaleAnimation.toValue = 1.0005;
            self.roundButton.layer.add(scaleAnimation, forKey: "scale")
            self.roundButton.showsTouchWhenHighlighted = true
        }
    }
    
    @objc func saveRegister() {
        globalRegisterReferenceVC!.sendRegisterClicked()
    }
    
    func createInterRegister() -> RegisterInter{
        var reg : RegisterInter
        var cociR: String = "0"
        var cociL: String = "0"
        var cociA: String = "0"
        var cociH: String = "0"
        var cociN: String = "0"
        var dormR: String = "0"
        var dormL: String = "0"
        var dormA: String = "0"
        var dormH: String = "0"
        var dormN: String = "0"
        var zonaR: String = "0"
        var zonaL: String = "0"
        var zonaA: String = "0"
        var zonaH: String = "0"
        var zonaN: String = "0"
        if globalCocinaVC != nil{
            cociR = (globalCocinaVC?.cocinaRastrosField.text!)!
            cociL = (globalCocinaVC?.cocinaLarvasField.text!)!
            cociA = (globalCocinaVC?.cocinaAdultosField.text!)!
            cociH = (globalCocinaVC?.cocinaHuevosField.text!)!
            cociN = (globalCocinaVC?.cocinaNinfasField.text!)!
        }
        if globalDormitorioVC != nil{
            dormR = (globalDormitorioVC?.dormitorioRastrosField.text!)!
            dormL = (globalDormitorioVC?.dormitorioLarvasField.text!)!
            dormA = (globalDormitorioVC?.dormitorioAdultosField.text!)!
            dormH = (globalDormitorioVC?.dormitorioHuevosField.text!)!
            dormN = (globalDormitorioVC?.dormitorioNinfasField.text!)!
        }
        if globalZonaVC != nil {
            zonaR = (globalZonaVC?.zonaSocialRastrosField.text!)!
            zonaL = (globalZonaVC?.zonaSocialLarvasField.text!)!
            zonaA = (globalZonaVC?.zonaSocialAdultosField.text!)!
            zonaH = (globalZonaVC?.zonaSocialHuevosField.text!)!
            zonaN = (globalZonaVC?.zonaSocialNinfasField.text!)!
        }
        reg = RegisterInter(rastroCocinaValue: cociR, huevosCocinaValue: cociH, larvasCocinaValue: cociL, ninfasCocinaValue: cociN, adultosCocinaValue: cociA, rastroDormitorioValue: dormR, huevosDormitorioValue: dormH, larvasDormitorioValue: dormL, ninfasDormitorioValue: dormN, adultosDormitorioValue: dormA, rastroZonaSocialValue: zonaR, huevosZonaSocialValue: zonaH, larvasZonaSocialValue: zonaL, ninfasZonaSocialValue: zonaN, adultosZonaSocialValue: zonaA)
        interRegisters.append(reg)
        return reg
    }
    
    func generateWarningLabel(){
        imageView.frame = CGRect(x: 335, y: 65, width: 30, height: 30)
        view.addSubview(imageView)
    }
    
    func deleteWarningLabel(){
        globalHomeVC!.emptyRegisters()
        imageView.removeFromSuperview()
    }
    

}
extension RegisterInterViewController: NetworkStatusListener {
    
    func networkStatusDidChange(status: Reachability.NetworkStatus) {
        
        switch status {
        case .notReachable:
            self.generateWarningLabel()
        case .reachableViaWiFi:
            self.deleteWarningLabel()
        case .reachableViaWWAN:
            self.deleteWarningLabel()
        }
 
    }
}
