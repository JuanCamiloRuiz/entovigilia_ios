//
//  createFBTableViewController.swift
//  EntoVigilia
//
//  Created by Juan Camilo Ruiz on 4/3/19.
//  Copyright © 2019 Juan Camilo Ruiz. All rights reserved.
//

import UIKit
import ReachabilitySwift
import FirebaseDatabase

class createFBTableViewController: UITableViewController {

    var refRegistros: DatabaseReference?
    let reachability = Reachability()!
    var registers:[PinRegisterSimplify] = []
    let imageView = UIImageView(image: UIImage(named: "noWifi.png"))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadRegisters()
        if reachability.currentReachabilityStatus == Reachability.NetworkStatus.reachableViaWiFi {
            deleteWarningLabel()
        } else if reachability.currentReachabilityStatus == Reachability.NetworkStatus.reachableViaWWAN {
            deleteWarningLabel()
        } else {
            generateWarningLabel()
        }
        
        refRegistros = Database.database().reference()
        refRegistros!.keepSynced(true)
    }
    
    private func loadRegisters(){
        let ref = Database.database().reference()
        let mapName = getSelectedMap()
        if mapName == ""{
            print("Error sacando los registros del mapa, su nombre es vacío")
            return
        }
        ref.child("mapas").child(mapName).child("hogares").observe(.childAdded, with: { (snapshot) in
            if let getData = snapshot.value as? [String:Any] {
                if let registrado = getData["registrado"] as? Int{
                if registrado == 0 {
                    self.addNewPin(data: getData)
                }
                } else {
                    self.showNotAbleToDrawPinsMessage()
                }
            }
        })
    }
    
    func showNotAbleToDrawPinsMessage(){
        let alert = UIAlertController(title: "Error al cargar algunos registros", message: "Algunos registros no se pueden mostrar debido a inconsistencias en los datos", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func getSelectedMap() -> String {
        if UserDefaults.standard.object(forKey: "mapName") != nil {
            for dictionary in UserDefaults.standard.object(forKey: "mapName") as! [[String:Any]]{
                return dictionary["name"] as! String
            }
        }
        return ""
    }
    
    func addNewPin(data: [String:Any]){
        print("Agregando new pin...")
        print(data)
        if let latitud = data["latitud"] as? Double, let longitud = data["longitud"] as? Double, let idHogar = data["idHogar"] as? String{
        let newPin: PinRegisterSimplify = PinRegisterSimplify(id: idHogar, latitud: latitud, longitud: longitud)
        registers.append(newPin)
        tableView.reloadData()
        } else {
            self.showNotAbleToDrawPinsMessage()
        }
    }
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSections: Int = 0
        if registers.count > 0
        {
            tableView.separatorStyle = .singleLine
            numOfSections            = 1
            tableView.backgroundView = nil
        }
        else
        {
            let noDataLabel: UILabel  = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text          = "No hay registros por hacer en la DB"
            noDataLabel.textColor     = UIColor.black
            noDataLabel.textAlignment = .center
            tableView.backgroundView  = noDataLabel
            tableView.separatorStyle  = .none
        }
        return numOfSections
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return registers.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "createFBTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? createFBTableViewCell
        let register = registers[indexPath.row]
        cell!.idLabel.text = "Id del hogar: " + String(register.id)
        cell!.latitudLabel.text = "Latitud: " + String(String(register.latitud).prefix(7))
        cell!.longitudLabel.text = "Longitud: " + String(String(register.longitud).prefix(7))
        cell!.createButton.tag = indexPath.row
        return cell!
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "createRegisterFB" {
            let destVC: TabbarViewController = segue.destination as! UITabBarController as! TabbarViewController
            let secondVC: RegisterReferenceViewController = destVC.viewControllers?.first as! RegisterReferenceViewController
            let button = sender as! UIButton
            let index = button.tag
            print(index)
            let thisRegister = registers[index]
            secondVC.latitudExistent = String(String(thisRegister.latitud).prefix(7))
            secondVC.longitudExistent =  String(String(thisRegister.longitud).prefix(7))
            secondVC.idExistent = thisRegister.id
        }/*
         else if segue.identifier == "backHome" {
         let destVC: UINavigationController = segue.destination as! UINavigationController
         let secondVC: HomeViewController = destVC.viewControllers.first as! HomeViewController
         secondVC.messageShowed = true
         }*/
    }
    
    override func viewWillAppear(_ animated: Bool) {
        ReachabilityManager.shared.addListener(listener: self)
        if reachability.currentReachabilityStatus == Reachability.NetworkStatus.reachableViaWiFi {
            deleteWarningLabel()
        } else if reachability.currentReachabilityStatus == Reachability.NetworkStatus.reachableViaWWAN {
            deleteWarningLabel()
        } else {
            generateWarningLabel()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        ReachabilityManager.shared.removeListener(listener: self)
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    func generateWarningLabel(){
        var headerView: UIView = UIView.init(frame: CGRect(origin: CGPoint(x: 1,y :50), size: CGSize(width: 276, height: 30)))
        imageView.frame = CGRect(x: 335, y: 0, width: 30, height: 30)
        headerView.addSubview(imageView)
        self.tableView.tableHeaderView = headerView
        /*
         warningLabel.center = CGPoint(x: 190, y: 15)
         warningLabel.textAlignment = .center
         warningLabel.text = "No hay conexión a internet"
         warningLabel.backgroundColor = UIColor.red
         warningLabel.layer.cornerRadius = 50
         self.view.addSubview(warningLabel)
         */
    }
    
    func deleteWarningLabel(){
        self.tableView.tableHeaderView = nil
        globalHomeVC!.emptyRegisters()
    }
    
}
extension createFBTableViewController: NetworkStatusListener {
    
    func networkStatusDidChange(status: Reachability.NetworkStatus) {
        
        switch status {
        case .notReachable:
            self.generateWarningLabel()
        case .reachableViaWiFi:
            self.deleteWarningLabel()
        case .reachableViaWWAN:
            self.deleteWarningLabel()
        }
        
    }
}
