//
//  DormitorioViewController.swift
//  EntoVigilia
//
//  Created by Juan Camilo Ruiz on 3/30/19.
//  Copyright © 2019 Juan Camilo Ruiz. All rights reserved.
//

import UIKit
import ReachabilitySwift
import AVFoundation

var globalDormitorioVC: DormitorioViewController?

class DormitorioViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, AVAudioRecorderDelegate, AVAudioPlayerDelegate{
    
    var interRegisters: [RegisterInter] = []
    private var roundButton = UIButton()
    var imagePickerController = UIImagePickerController()
    var imagePicked = 0
    let imageView = UIImageView(image: UIImage(named: "noWifi.png"))
    let reachability = Reachability()!
    var dormitorioHasImage = false
    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    var audioPlayer:AVAudioPlayer!
    
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var dormitorioImage: UIImageView!
    @IBOutlet weak var dormitorioRastrosField: UITextField!
    @IBOutlet weak var dormitorioHuevosField: UITextField!
    @IBOutlet weak var dormitorioLarvasField: UITextField!
    @IBOutlet weak var dormitorioNinfasField: UITextField!
    @IBOutlet weak var dormitorioAdultosField: UITextField!
    @IBOutlet weak var dormitorioView: UIView!
    @IBOutlet weak var takePhotoButton: UIButton!
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var playButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        globalDormitorioVC = self
        
        self.hideKeyboardWhenTappedAround()
        createFlashButton()
        
        dormitorioAdultosField!.delegate = self
        dormitorioNinfasField!.delegate = self
        dormitorioLarvasField!.delegate = self
        dormitorioHuevosField!.delegate = self
        dormitorioRastrosField!.delegate = self
        
        if UserDefaults.standard.object(forKey: "dormitorioValues") != nil {
            for dictionary in UserDefaults.standard.object(forKey: "dormitorioValues") as! [[String:String]]{
                dormitorioAdultosField!.text = dictionary["adultos"]
                dormitorioNinfasField!.text = dictionary["ninfas"]
                dormitorioLarvasField!.text = dictionary["larvas"]
                dormitorioHuevosField!.text = dictionary["huevos"]
                dormitorioRastrosField!.text = dictionary["rastros"]
            }
        } else {
            dormitorioAdultosField!.text = "0"
            dormitorioNinfasField!.text = "0"
            dormitorioLarvasField!.text = "0"
            dormitorioHuevosField!.text = "0"
            dormitorioRastrosField!.text = "0"
        }

        dormitorioAdultosField!.tag = 9
        dormitorioNinfasField!.tag = 8
        dormitorioLarvasField!.tag = 7
        dormitorioHuevosField!.tag = 6
        dormitorioRastrosField!.tag = 5
        imagePickerController.sourceType = .camera
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = false
        
        makeRounded()
        
        dormitorioRastrosField!.keyboardType = UIKeyboardType.decimalPad
        dormitorioNinfasField!.keyboardType = UIKeyboardType.decimalPad
        dormitorioLarvasField!.keyboardType = UIKeyboardType.decimalPad
        dormitorioHuevosField!.keyboardType = UIKeyboardType.decimalPad
        dormitorioRastrosField!.keyboardType = UIKeyboardType.decimalPad
        dormitorioAdultosField!.keyboardType = UIKeyboardType.decimalPad
        
        dormitorioRastrosField.addDoneCancelToolbar(onDone: (target: self, action: #selector(textFieldDone)))
        dormitorioHuevosField.addDoneCancelToolbar(onDone: (target: self, action: #selector(textFieldDone)))
        dormitorioNinfasField.addDoneCancelToolbar(onDone: (target: self, action: #selector(textFieldDone)))
        dormitorioAdultosField.addDoneCancelToolbar(onDone: (target: self, action: #selector(textFieldDone)))
        dormitorioLarvasField.addDoneCancelToolbar(onDone: (target: self, action: #selector(textFieldDone)))
        
        // setup keyboard event
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name:UIResponder.keyboardWillHideNotification, object: nil)
        setImage()
        recordingSession = AVAudioSession.sharedInstance()
        
        do {
            try recordingSession.setCategory(.playAndRecord, mode: .default)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        self.loadRecordingUI()
                    } else {
                        // failed to record!
                    }
                }
            }
        } catch {
            // failed to record!
        }
        let filemanager = FileManager.default
        if  filemanager.fileExists(atPath: getDocumentsDirectory().appendingPathComponent("dormAudio.m4a").path){
            recordButton.setTitle("Borrar audio",for: .normal)
            recordButton.tintColor = .red
            recordButton.borderColor = .red
            playButton.isEnabled = true
        }
    }
    
    func createFlashButton() {
        roundButton = UIButton(type: .custom)
        roundButton.translatesAutoresizingMaskIntoConstraints = false
        // roundButton.backgroundColor = .green
        // Make sure you replace the name of the image:
        roundButton.setImage(UIImage(named:"flashlight"), for: .normal)
        // Make sure to create a function and replace DOTHISONTAP with your own function:
        roundButton.addTarget(self, action: #selector(flashLigthTapped), for: UIControl.Event.touchUpInside)
        // We're manipulating the UI, must be on the main thread:
        DispatchQueue.main.async {
            if let keyWindow = UIApplication.shared.keyWindow {
                keyWindow.addSubview(self.roundButton)
                NSLayoutConstraint.activate([
                    keyWindow.trailingAnchor.constraint(equalTo: self.roundButton.trailingAnchor, constant: 15),
                    keyWindow.bottomAnchor.constraint(equalTo: self.roundButton.bottomAnchor, constant: 600),
                    self.roundButton.widthAnchor.constraint(equalToConstant: 45),
                    self.roundButton.heightAnchor.constraint(equalToConstant: 45)])
            }
            // Make the button round:
            self.roundButton.layer.cornerRadius = 26
        }
    }
    
    @objc func flashLigthTapped() {
        guard let device = AVCaptureDevice.default(for: AVMediaType.video) else { return }
        guard device.hasTorch else { return }
        
        do {
            try device.lockForConfiguration()
            
            if (device.torchMode == AVCaptureDevice.TorchMode.on) {
                device.torchMode = AVCaptureDevice.TorchMode.off
            } else {
                do {
                    try device.setTorchModeOn(level: 1.0)
                } catch {
                    print(error)
                }
            }
            
            device.unlockForConfiguration()
        } catch {
            print(error)
        }
    }
    
    func loadRecordingUI() {
        recordButton.addTarget(self, action: #selector(recordTapped), for: .touchUpInside)
    }
    
    func startRecording() {
        let audioFilename = getDocumentsDirectory().appendingPathComponent("dormAudio.m4a")
        
        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
        
        do {
            audioRecorder = try AVAudioRecorder(url: audioFilename, settings: settings)
            audioRecorder.delegate = self
            audioRecorder.record()
            
            recordButton.setTitle("Detener grabación", for: .normal)
        } catch {
            finishRecording(success: false)
        }
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func finishRecording(success: Bool) {
        audioRecorder.stop()
        
        if success {
            recordButton.setTitle("Borrar audio",for: .normal)
            recordButton.tintColor = .red
            recordButton.borderColor = .red
            playButton.isEnabled = true
        } else {
            recordButton.setTitle("Grabar audio", for: .normal)
            // recording failed :(
        }
    }
    
    @objc func recordTapped() {
        if recordButton.titleLabel?.text == "Grabar audio" {
            startRecording()
        } else if  recordButton.titleLabel?.text == "Borrar audio"{
            let document = try!
                FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            do {
                try FileManager.default.removeItem(at: document.appendingPathComponent("dormAudio.m4a", isDirectory: true))
                playButton.isEnabled = false
                recordButton.setTitle("Grabar audio", for: .normal)
                recordButton.tintColor = UIColor(red: 11/255, green: 201/255, blue: 8/255, alpha: 1.0)
                recordButton.borderColor = UIColor(red: 11/255, green: 201/255, blue: 8/255, alpha: 1.0)
            }
            catch{
                print("There was not an cocina audio")
            }
        } else {
            finishRecording(success: true)
        }
    }
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }
    }
    
    
    @IBAction func playButtonTapped(_ sender: UIButton) {
        if (sender.titleLabel?.text == "Reproducir"){
            recordButton.isEnabled = false
            sender.setTitle("Detener", for: .normal)
            sender.setImage(UIImage(named: "pause.png"), for: .normal)
            preparePlayer()
            audioPlayer.play()
        } else {
            audioPlayer.stop()
            sender.setTitle("Reproducir", for: .normal)
            sender.setImage(UIImage(named: "play.png"), for: .normal)
        }
        recordButton.isEnabled = true
    }
    
    func preparePlayer() {
        var error: NSError?
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: getDocumentsDirectory().appendingPathComponent("dormAudio.m4a") as URL)
        } catch let error1 as NSError {
            error = error1
            audioPlayer = nil
        }
        
        if let err = error {
            print("AVAudioPlayer error: \(err.localizedDescription)")
        } else {
            audioPlayer.delegate = self
            audioPlayer.prepareToPlay()
            audioPlayer.volume = 10.0
        }
    }
    
    func getTotalVectors() -> String{
        let value1: Int? = Int(dormitorioLarvasField.text!)
        let value2: Int? = Int(dormitorioHuevosField.text!)
        let value3: Int? = Int(dormitorioAdultosField.text!)
        let value4: Int? = Int(dormitorioNinfasField.text!)
        let value5: Int? = Int(dormitorioRastrosField.text!)
        let returnValue = value1! + value2! + value3! + value4! + value5!
        return String(returnValue)
    }
    
    func ThereIsImage() -> Bool {
        if dormitorioImage.image != nil{
            return true
        } else {
            let document = try!
                FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            let fooURL = document.appendingPathComponent("imgDormitorio.png")
            let fileExists = FileManager().fileExists(atPath: fooURL.path)
            return fileExists
        }
    }
    
    func ThereIsAudio() -> Bool {
        let filemanager = FileManager.default
        return filemanager.fileExists(atPath: getDocumentsDirectory().appendingPathComponent("dormAudio.m4a").path)
    }
    
    @objc func textFieldDone(){
        self.view.endEditing(true);
    }
    
    func makeRounded() {
        dormitorioView.layer.cornerRadius = 5
        dormitorioView.layer.shadowColor = UIColor.black.cgColor
        dormitorioView.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        dormitorioView.layer.masksToBounds = false
        dormitorioView.layer.shadowRadius = 2.0
        dormitorioView.layer.shadowOpacity = 0.5
        
        dormitorioImage.layer.masksToBounds = false
        dormitorioImage.layer.cornerRadius = 10
        dormitorioImage.clipsToBounds = true
    }
    
    func resetValues(){
        dormitorioAdultosField!.text = "0"
        dormitorioNinfasField!.text = "0"
        dormitorioLarvasField!.text = "0"
        dormitorioHuevosField!.text = "0"
        dormitorioRastrosField!.text = "0"
        dormitorioImage.image = nil
    }
    func saveImages(id: String){
        let document = try!
            FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let oldPath = document.appendingPathComponent("imgDormitorio.png", isDirectory: true)
        let nameImgDorm = "imgDormitorio" + id + ".png"
        let imgDormitorioUrl = document.appendingPathComponent(nameImgDorm, isDirectory: true)
        print(dormitorioHasImage)
        if dormitorioImage.image != nil{
            do{
                if dormitorioImage.image?.pngData() != nil{
                try FileManager.default.copyItem(atPath: oldPath.path, toPath: imgDormitorioUrl.path)
                print("Dormitorio image saved")
                }
            } catch {
                print("Error saving dormitorio image")
            }
        }
        dormitorioHasImage = false
        dormitorioImage.image = nil
    }
    
    @objc func keyboardWillShow(notification:NSNotification){
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.scrollview.contentInset
        contentInset.bottom = keyboardFrame.size.height + 50
        scrollview.contentInset = contentInset
    }
    
    @objc func keyboardWillHide(notification:NSNotification){
        
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        scrollview.contentInset = contentInset
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1
        
        if let nextResponder = textField.superview?.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        createFlashButton()
        ReachabilityManager.shared.addListener(listener: self)
        if reachability.currentReachabilityStatus == Reachability.NetworkStatus.reachableViaWiFi {
            deleteWarningLabel()
        } else if reachability.currentReachabilityStatus == Reachability.NetworkStatus.reachableViaWWAN {
            deleteWarningLabel()
        } else {
            generateWarningLabel()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        if roundButton.superview != nil {
            DispatchQueue.main.async {
                self.roundButton.removeFromSuperview()
                self.roundButton.isEnabled = false
                self.roundButton.isHidden = true
            }
        }
        ReachabilityManager.shared.removeListener(listener: self)
        
        let cocinaDictionary:[String:String] = ["rastros":dormitorioRastrosField.text!,"huevos":dormitorioHuevosField.text!,"adultos":dormitorioAdultosField.text!, "ninfas": dormitorioNinfasField.text!, "larvas": dormitorioLarvasField.text!]
        var cocinaArray = [[String:String]]()
        cocinaArray.append(cocinaDictionary)
        UserDefaults.standard.set(cocinaArray, forKey: "dormitorioValues")
        UserDefaults.standard.synchronize()
        fillValues()
    }
    
    func fillValues(){
        if dormitorioAdultosField.text!.isEmpty {
           dormitorioAdultosField!.text = "0"
        }
        if dormitorioNinfasField.text!.isEmpty {
            dormitorioNinfasField!.text = "0"
        }
        if dormitorioLarvasField.text!.isEmpty {
           dormitorioLarvasField!.text = "0"
        }
        if dormitorioHuevosField.text!.isEmpty {
            dormitorioHuevosField!.text = "0"
        }
        if dormitorioRastrosField.text!.isEmpty {
            dormitorioRastrosField!.text = "0"
        }
    }
    
    func temporalySaveImage(){
        let document = try!
            FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        print(document)
        let nameImgCocina = "imgDormitorio.png"
        let imgCocinaUrl = document.appendingPathComponent(nameImgCocina, isDirectory: true)
        if dormitorioImage.image != nil{
            do{
                try dormitorioImage.image?.pngData()?.write(to: imgCocinaUrl)
            } catch {
                print("Error saving dorm image")
            }
        }
    }
    
    func saveAudios(id: String){
        let oldPath = getDocumentsDirectory().appendingPathComponent("dormAudio.m4a")
        let document = try!
            FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let nameAudio = "dormAudio" + id + ".m4a"
        let audioURL = document.appendingPathComponent(nameAudio, isDirectory: true)
        if  ThereIsAudio() {
            do {
                try FileManager.default.copyItem(atPath: oldPath.path, toPath: audioURL.path)
            }
            catch{
                print(error)
            }
        }
    }
    
    func setImage(){
        let document = try!
            FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let nameImg = "imgDormitorio.png"
        let imgUrl = document.appendingPathComponent(nameImg, isDirectory: true)
        dormitorioImage.image = UIImage(contentsOfFile: imgUrl.path)
        dormitorioImage.isHidden = false
        if dormitorioImage.image != nil{
            takePhotoButton.setTitle("Borrar foto",for: .normal)
            takePhotoButton.tintColor = .red
            takePhotoButton.borderColor = .red
        }
    }
    
    @IBAction func imageTapped(_ sender: UITapGestureRecognizer) {
        let imageView = sender.view as! UIImageView
        let newImageView = UIImageView(image: imageView.image)
        newImageView.frame = UIScreen.main.bounds
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        newImageView.addGestureRecognizer(tap)
        self.view.addSubview(newImageView)
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = true
    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        self.navigationController?.isNavigationBarHidden = false
        self.tabBarController?.tabBar.isHidden = false
        sender.view?.removeFromSuperview()
    }
    
    @IBAction func selectDormitorioImage(_ sender: UIButton) {
        if sender.titleLabel?.text == "Tomar foto" {
            imagePicked = 2
            dormitorioHasImage = true
            present(imagePickerController, animated: true)
        } else if sender.titleLabel?.text == "Borrar foto" {
            dormitorioHasImage = false
            dormitorioImage.image = nil
            let document = try!
                FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            do {
                try FileManager.default.removeItem(at: document.appendingPathComponent("imgDormitorio.png", isDirectory: true))
            }
            catch{
            }
            sender.setTitle("Tomar foto",for: .normal)
            sender.tintColor = UIColor(red: 11/255, green: 201/255, blue: 8/255, alpha: 1.0)
            sender.borderColor = UIColor(red: 11/255, green: 201/255, blue: 8/255, alpha: 1.0)
            dormitorioImage.isHidden = true
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        // Dismiss the picker if the user canceled.
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // The info dictionary may contain multiple representations of the image. You want to use the original.
        guard let selectedImage = info[.originalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        dormitorioImage.image = resize(selectedImage)
        takePhotoButton.setTitle("Borrar foto",for: .normal)
        takePhotoButton.tintColor = .red
        takePhotoButton.borderColor = .red
        // Dismiss the picker.
        dismiss(animated: true, completion: nil)
        dormitorioImage.isHidden = false
        temporalySaveImage()
    }
    
    func resize(_ image: UIImage) -> UIImage {
        var actualHeight = Float(image.size.height)
        var actualWidth = Float(image.size.width)
        let maxHeight: Float = 300.0
        let maxWidth: Float = 400.0
        var imgRatio: Float = actualWidth / actualHeight
        let maxRatio: Float = maxWidth / maxHeight
        let compressionQuality: Float = 1.0
        //50 percent compression
        if actualHeight > maxHeight || actualWidth > maxWidth {
            if imgRatio < maxRatio {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if imgRatio > maxRatio {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }
            else {
                actualHeight = maxHeight
                actualWidth = maxWidth
            }
        }
        let rect = CGRect(x: 0.0, y: 0.0, width: CGFloat(actualWidth), height: CGFloat(actualHeight))
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        let imageData = img?.jpegData(compressionQuality: CGFloat(compressionQuality))
        UIGraphicsEndImageContext()
        return UIImage(data: imageData!) ?? UIImage()
    }
        
    @objc func saveRegister() {
        globalRegisterReferenceVC!.sendRegisterClicked()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if newString.count > 4 {
            return false
        }
        
        return newString.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
    }
    
    func generateWarningLabel(){
        imageView.frame = CGRect(x: 335, y: 65, width: 30, height: 30)
        view.addSubview(imageView)
    }
    
    func deleteWarningLabel(){
        globalHomeVC!.emptyRegisters()
        imageView.removeFromSuperview()
    }
}
extension DormitorioViewController: NetworkStatusListener {
    
    func networkStatusDidChange(status: Reachability.NetworkStatus) {
        
        switch status {
        case .notReachable:
            self.generateWarningLabel()
        case .reachableViaWiFi:
            self.deleteWarningLabel()
        case .reachableViaWWAN:
            self.deleteWarningLabel()
        }
        
    }
}
