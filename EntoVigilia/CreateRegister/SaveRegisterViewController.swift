//
//  SaveRegisterViewController.swift
//  EntoVigilia
//
//  Created by Juan Camilo Ruiz on 3/24/19.
//  Copyright © 2019 Juan Camilo Ruiz. All rights reserved.
//

import UIKit
import ReachabilitySwift
import Firebase

class SaveRegisterViewController: UIViewController {

    let imageView = UIImageView(image: UIImage(named: "noWifi.png"))
    let reachability = Reachability()!
    private var roundButton = UIButton()
    var theresConection = true
    
    
    @IBOutlet weak var latitudLabel: UITextField!
    @IBOutlet weak var longitudLabel: UITextField!
    @IBOutlet weak var deptoLabel: UITextField!
    @IBOutlet weak var munLabel: UITextField!
    @IBOutlet weak var veredaLabel: UITextField!
    @IBOutlet weak var idLabel: UITextField!
    @IBOutlet weak var RefView: UIView!
    @IBOutlet weak var interView: UIView!
    @IBOutlet weak var periView: UIView!
    
    var actualRegister: Register?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadRegister()
        //makeRounded()
        // Do any additional setup after loading the view.
    }
    
    @objc func saveRegister(_ sender: UIBarButtonItem) {
        displayCreatedMessage(id: (actualRegister?.refRegister.id)!)
    }
    
    
    private func loadRegister(){
        actualRegister = globalRegisterReferenceVC!.actualRegister
        var referenceRegister: RegisterReference
        referenceRegister = actualRegister!.refRegister
        latitudLabel.text = String(referenceRegister.latitud)
        longitudLabel.text = String(referenceRegister.longitud)
        deptoLabel.text = referenceRegister.departamento
        munLabel.text = referenceRegister.municipio
        veredaLabel.text = referenceRegister.vereda
        idLabel.text = String(referenceRegister.id)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.roundButton.isEnabled = true
        self.roundButton.isHidden = false
        createFloatingButton()
        ReachabilityManager.shared.addListener(listener: self)
        if reachability.currentReachabilityStatus == Reachability.NetworkStatus.reachableViaWiFi {
            theresConection = true
            deleteWarningLabel()
        } else if reachability.currentReachabilityStatus == Reachability.NetworkStatus.reachableViaWWAN {
            theresConection = true
            deleteWarningLabel()
        } else {
            theresConection = false
            generateWarningLabel()
            
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if roundButton.superview != nil {
            DispatchQueue.main.async {
                self.roundButton.removeFromSuperview()
                self.roundButton.isEnabled = false
                self.roundButton.isHidden = true
            }
        }
        ReachabilityManager.shared.removeListener(listener: self)
    }
    
    func createFloatingButton() {
        roundButton = UIButton(type: .custom)
        roundButton.translatesAutoresizingMaskIntoConstraints = false
        roundButton.backgroundColor = UIColor(red: 11/255, green: 201/255, blue: 8/255, alpha: 1.0)
        // Make sure you replace the name of the image:
        roundButton.setImage(UIImage(named:"sendImage"), for: .normal)
        // Make sure to create a function and replace DOTHISONTAP with your own function:
        roundButton.addTarget(self, action: #selector(saveRegister), for: UIControl.Event.touchUpInside)
        // We're manipulating the UI, must be on the main thread:
        DispatchQueue.main.async {
            if let keyWindow = UIApplication.shared.keyWindow {
                keyWindow.addSubview(self.roundButton)
                NSLayoutConstraint.activate([
                    keyWindow.trailingAnchor.constraint(equalTo: self.roundButton.trailingAnchor, constant: 15),
                    keyWindow.bottomAnchor.constraint(equalTo: self.roundButton.bottomAnchor, constant: 50),
                    self.roundButton.widthAnchor.constraint(equalToConstant: 50),
                    self.roundButton.heightAnchor.constraint(equalToConstant: 50)])
            }
            // Make the button round:
            self.roundButton.layer.cornerRadius = 26
            // Add a black shadow:
            self.roundButton.layer.shadowColor = UIColor.black.cgColor
            self.roundButton.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
            self.roundButton.layer.masksToBounds = false
            self.roundButton.layer.shadowRadius = 2.0
            self.roundButton.layer.shadowOpacity = 0.5
            // Add a pulsing animation to draw attention to button:
            let scaleAnimation: CABasicAnimation = CABasicAnimation(keyPath: "transform.scale")
            scaleAnimation.duration = 0.4
            scaleAnimation.repeatCount = .greatestFiniteMagnitude
            scaleAnimation.autoreverses = true
            scaleAnimation.fromValue = 1.0;
            scaleAnimation.toValue = 1.0005;
            self.roundButton.layer.add(scaleAnimation, forKey: "scale")
            self.roundButton.showsTouchWhenHighlighted = true
        }
    }
    
    func makeRounded() {
        RefView.layer.cornerRadius = 5
        RefView.layer.shadowColor = UIColor.black.cgColor
        RefView.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        RefView.layer.masksToBounds = false
        RefView.layer.shadowRadius = 2.0
        RefView.layer.shadowOpacity = 0.5
        
        interView.layer.cornerRadius = 5
        interView.layer.shadowColor = UIColor.black.cgColor
        interView.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        interView.layer.masksToBounds = false
        interView.layer.shadowRadius = 2.0
        interView.layer.shadowOpacity = 0.5
        
        periView.layer.cornerRadius = 5
        periView.layer.shadowColor = UIColor.black.cgColor
        periView.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        periView.layer.masksToBounds = false
        periView.layer.shadowRadius = 2.0
        periView.layer.shadowOpacity = 0.5
    }
    
    func displayCreatedMessage(id: String){
        var userMessage: String = "¿ Estas seguro que deseas crear el registro con id " + id + " ?"
        if Auth.auth().currentUser != nil{
            if Auth.auth().currentUser!.isEmailVerified{
            } else {
                 userMessage = "Este registro no se subira a la base de datos hasta que verifiques el email de tu cuenta"
            }
        }
        let myAlert = UIAlertController(title: "¿Crear registro?", message: userMessage, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "Si, estoy seguro", style: UIAlertAction.Style.default, handler: {_ in
            globalRegisterReferenceVC!.updateRegisters(register: self.actualRegister!)
            self.sendToHome()
        })
        let cancelAction = UIAlertAction(title: "No, cancelar", style: .cancel) { (action:UIAlertAction!) in
        }
        myAlert.addAction(cancelAction)
        myAlert.addAction(okAction)
        self.present(myAlert, animated: true, completion: nil)
            
    }
    
    func sendToHome(){
        performSegue(withIdentifier: "GoHomeView", sender: self)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showResumeCocina" {
            let detailVC: CocinaDetailViewController = segue.destination as! CocinaDetailViewController
            detailVC.actualRegister = actualRegister
            detailVC.titleRegister = "Cocina"
        } else if segue.identifier == "showResumeDorm" {
            let detailVC: CocinaDetailViewController = segue.destination as! CocinaDetailViewController
            detailVC.actualRegister = actualRegister
            detailVC.titleRegister = "Dormitorio"
        } else if segue.identifier == "showResumeZona" {
            let detailVC: CocinaDetailViewController = segue.destination as! CocinaDetailViewController
            detailVC.actualRegister = actualRegister
            detailVC.titleRegister = "ZonaSocial"
        } else if segue.identifier == "showResumeGall" {
            let detailVC: CocinaDetailViewController = segue.destination as! CocinaDetailViewController
            detailVC.actualRegister = actualRegister
            detailVC.titleRegister = "Gallinero"
        } else if segue.identifier == "showResumeCorral" {
            let detailVC: CocinaDetailViewController = segue.destination as! CocinaDetailViewController
            detailVC.actualRegister = actualRegister
            detailVC.titleRegister = "Corral"
        } else if segue.identifier == "showResumeDeposito" {
            let detailVC: CocinaDetailViewController = segue.destination as! CocinaDetailViewController
            detailVC.actualRegister = actualRegister
            detailVC.titleRegister = "Deposito"
        }
    }
    
    func generateWarningLabel(){
        imageView.frame = CGRect(x: 335, y: 65, width: 30, height: 30)
        view.addSubview(imageView)
    }
    
    func deleteWarningLabel(){
        globalHomeVC!.emptyRegisters()
        imageView.removeFromSuperview()
    }
    
}
extension SaveRegisterViewController: NetworkStatusListener {
    
    func networkStatusDidChange(status: Reachability.NetworkStatus) {
        
        
        switch status {
        case .notReachable:
            theresConection = false
            self.generateWarningLabel()
        case .reachableViaWiFi:
            theresConection = true
            self.deleteWarningLabel()
        case .reachableViaWWAN:
            theresConection = true
            self.deleteWarningLabel()
        }
        
    }
}
