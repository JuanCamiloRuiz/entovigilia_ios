//
//  RegisterViewController.swift
//  EntoVigilia
//
//  Created by Juan Camilo Ruiz on 3/15/19.
//  Copyright © 2019 Juan Camilo Ruiz. All rights reserved.
//

import UIKit
import os.log
import ReachabilitySwift
import AVFoundation

var globalRegisterPeroVC: RegisterPeroViewController?

class RegisterPeroViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate{

    var peroRegisters: [RegisterPero] = []
    var registers: [Register] = []
    private var roundButton = UIButton()
    var imagePickerController = UIImagePickerController()
    var imagePicked = 0
    let imageView = UIImageView(image: UIImage(named: "noWifi.png"))
    let reachability = Reachability()!
    private var roundFlashButton = UIButton()
    
    @IBOutlet weak var gallineroVectoresLabel: UILabel!
    @IBOutlet weak var corralVectoresLabel: UILabel!
    @IBOutlet weak var depositoVectoresLabel: UILabel!
    @IBOutlet weak var gallineroFotoExist: UIImageView!
    @IBOutlet weak var corralFotoExist: UIImageView!
    @IBOutlet weak var depositoFotoExist: UIImageView!
    @IBOutlet weak var gallineroAudioExist: UIImageView!
    @IBOutlet weak var corralAudioExist: UIImageView!
    @IBOutlet weak var depositoAudioExist: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        globalRegisterPeroVC = self
        createFlashButton()
    }
    
    func createFlashButton() {
        roundFlashButton = UIButton(type: .custom)
        roundFlashButton.translatesAutoresizingMaskIntoConstraints = false
        // roundFlashButton.backgroundColor = .green
        // Make sure you replace the name of the image:
        roundFlashButton.setImage(UIImage(named:"flashlight"), for: .normal)
        // Make sure to create a function and replace DOTHISONTAP with your own function:
        roundFlashButton.addTarget(self, action: #selector(flashLigthTapped), for: UIControl.Event.touchUpInside)
        // We're manipulating the UI, must be on the main thread:
        DispatchQueue.main.async {
            if let keyWindow = UIApplication.shared.keyWindow {
                keyWindow.addSubview(self.roundFlashButton)
                NSLayoutConstraint.activate([
                    keyWindow.trailingAnchor.constraint(equalTo: self.roundFlashButton.trailingAnchor, constant: 15),
                    keyWindow.bottomAnchor.constraint(equalTo: self.roundFlashButton.bottomAnchor, constant: 600),
                    self.roundFlashButton.widthAnchor.constraint(equalToConstant: 45),
                    self.roundFlashButton.heightAnchor.constraint(equalToConstant: 45)])
            }
            // Make the button round:
            self.roundFlashButton.layer.cornerRadius = 26
        }
    }
    
    @objc func flashLigthTapped() {
        guard let device = AVCaptureDevice.default(for: AVMediaType.video) else { return }
        guard device.hasTorch else { return }
        
        do {
            try device.lockForConfiguration()
            
            if (device.torchMode == AVCaptureDevice.TorchMode.on) {
                device.torchMode = AVCaptureDevice.TorchMode.off
            } else {
                do {
                    try device.setTorchModeOn(level: 1.0)
                } catch {
                    print(error)
                }
            }
            
            device.unlockForConfiguration()
        } catch {
            print(error)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1
        
        if let nextResponder = textField.superview?.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.roundButton.isEnabled = true
        self.roundButton.isHidden = false
        createFloatingButton()
        createFlashButton()
        ReachabilityManager.shared.addListener(listener: self)
        if reachability.currentReachabilityStatus == Reachability.NetworkStatus.reachableViaWiFi {
            deleteWarningLabel()
        } else if reachability.currentReachabilityStatus == Reachability.NetworkStatus.reachableViaWWAN {
            deleteWarningLabel()
        } else {
            generateWarningLabel()
        }
        updateStuff() 
    }
    
    func updateStuff(){
        if globalGallineroVC != nil {
            gallineroVectoresLabel.text = String("Vectores totales: ") + (globalGallineroVC?.getTotalVectors())!
            if globalGallineroVC!.ThereIsImage() {
                gallineroFotoExist.image = UIImage(named: "check.png")
            } else {
                gallineroFotoExist.image = UIImage(named: "wrong.png")
            }
            if globalGallineroVC!.ThereIsAudio() {
                gallineroAudioExist.image = UIImage(named: "check.png")
            } else {
                gallineroAudioExist.image = UIImage(named: "wrong.png")
            }
        }
        if globalCorralVC != nil {
            corralVectoresLabel.text = String("Vectores totales: ") + (globalCorralVC?.getTotalVectors())!
            if globalCorralVC!.ThereIsImage() {
                corralFotoExist.image = UIImage(named: "check.png")
            } else {
                corralFotoExist.image = UIImage(named: "wrong.png")
            }
            if globalCorralVC!.ThereIsAudio() {
                corralAudioExist.image = UIImage(named: "check.png")
            } else {
                corralAudioExist.image = UIImage(named: "wrong.png")
            }
        }
        if globalDepositoVC != nil {
            depositoVectoresLabel.text = String("Vectores totales: ") + (globalDepositoVC?.getTotalVectors())!
            if globalDepositoVC!.ThereIsImage() {
                depositoFotoExist.image = UIImage(named: "check.png")
            } else {
                depositoFotoExist.image = UIImage(named: "wrong.png")
            }
            if globalDepositoVC!.ThereIsAudio() {
                depositoAudioExist.image = UIImage(named: "check.png")
            } else {
                depositoAudioExist.image = UIImage(named: "wrong.png")
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        if roundButton.superview != nil {
            DispatchQueue.main.async {
                self.roundButton.removeFromSuperview()
                self.roundButton.isEnabled = false
                self.roundButton.isHidden = true
            }
        }
        
        if roundFlashButton.superview != nil {
            DispatchQueue.main.async {
                self.roundFlashButton.removeFromSuperview()
                self.roundFlashButton.isEnabled = false
                self.roundFlashButton.isHidden = true
            }
        }
        
        ReachabilityManager.shared.removeListener(listener: self)
    }
    
    func createFloatingButton() {
        roundButton = UIButton(type: .custom)
        roundButton.translatesAutoresizingMaskIntoConstraints = false
        roundButton.backgroundColor = UIColor(red: 11/255, green: 201/255, blue: 8/255, alpha: 1.0)
        // Make sure you replace the name of the image:
        roundButton.setImage(UIImage(named:"sendImage"), for: .normal)
        // Make sure to create a function and replace DOTHISONTAP with your own function:
        roundButton.addTarget(self, action: #selector(saveRegister), for: UIControl.Event.touchUpInside)
        // We're manipulating the UI, must be on the main thread:
        DispatchQueue.main.async {
            if let keyWindow = UIApplication.shared.keyWindow {
                keyWindow.addSubview(self.roundButton)
                NSLayoutConstraint.activate([
                    keyWindow.trailingAnchor.constraint(equalTo: self.roundButton.trailingAnchor, constant: 15),
                    keyWindow.bottomAnchor.constraint(equalTo: self.roundButton.bottomAnchor, constant: 50),
                    self.roundButton.widthAnchor.constraint(equalToConstant: 50),
                    self.roundButton.heightAnchor.constraint(equalToConstant: 50)])
            }
            // Make the button round:
            self.roundButton.layer.cornerRadius = 26
            // Add a black shadow:
            self.roundButton.layer.shadowColor = UIColor.black.cgColor
            self.roundButton.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
            self.roundButton.layer.masksToBounds = false
            self.roundButton.layer.shadowRadius = 2.0
            self.roundButton.layer.shadowOpacity = 0.5
            // Add a pulsing animation to draw attention to button:
            let scaleAnimation: CABasicAnimation = CABasicAnimation(keyPath: "transform.scale")
            scaleAnimation.duration = 0.4
            scaleAnimation.repeatCount = .greatestFiniteMagnitude
            scaleAnimation.autoreverses = true
            scaleAnimation.fromValue = 1.0;
            scaleAnimation.toValue = 1.0005;
            self.roundButton.layer.add(scaleAnimation, forKey: "scale")
            self.roundButton.showsTouchWhenHighlighted = true
        }
    }

    func saveImages(id: String){
        if globalGallineroVC != nil {
           globalGallineroVC?.saveImages(id: id)
        }
        if globalCorralVC != nil {
            globalCorralVC?.saveImages(id: id)
        }
        if globalDepositoVC != nil {
            globalDepositoVC?.saveImages(id: id)
        }
    }
    
    func saveAudios(id: String){
        if globalGallineroVC != nil {
            globalGallineroVC?.saveAudios(id: id)
        }
        if globalCorralVC != nil {
            globalCorralVC?.saveAudios(id: id)
        }
        if globalDepositoVC != nil {
            globalDepositoVC?.saveAudios(id: id)
        }
    }
    
    @objc func saveRegister() {
        globalRegisterReferenceVC!.sendRegisterClicked()
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        // Dismiss the picker if the user canceled.
        dismiss(animated: true, completion: nil)
    }
    
    /*
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // The info dictionary may contain multiple representations of the image. You want to use the original.
        guard let selectedImage = info[.originalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
            }
        
        if imagePicked == 1 {
            gallineroImage.image = selectedImage
        } else if imagePicked == 2 {
            corralImage.image = selectedImage
        } else if imagePicked == 3{
            depositoImage.image = selectedImage
        }
        
        // Dismiss the picker.
        dismiss(animated: true, completion: nil)
    }*/
    
    
    private func saveRegisters(regs: [Register]) {
        do {
            let data = try PropertyListEncoder().encode(regs)
            let success = NSKeyedArchiver.archiveRootObject(data, toFile: Register.ArchiveURL.path)
            print(success ? "Successful save" : "Save Failed")
        } catch {
            print("Save Failed")
        }
    }
    
    func retrieveRegisters() -> [Register]? {
        guard let data = NSKeyedUnarchiver.unarchiveObject(withFile: Register.ArchiveURL.path) as? Data else { return nil }
        do {
            let registers = try PropertyListDecoder().decode([Register].self, from: data)
            return registers
        } catch {
            print("Retrieve Failed")
            return nil
        }
    }
    
    
    func createPeroRegister() -> RegisterPero{
        var reg : RegisterPero
        var gallR: String = "0"
        var gallL: String = "0"
        var gallA: String = "0"
        var gallH: String = "0"
        var gallN: String = "0"
        var corrR: String = "0"
        var corrL: String = "0"
        var corrA: String = "0"
        var corrH: String = "0"
        var corrN: String = "0"
        var depoR: String = "0"
        var depoL: String = "0"
        var depoA: String = "0"
        var depoH: String = "0"
        var depoN: String = "0"
        if globalGallineroVC != nil{
            gallR = (globalGallineroVC?.gallineroRastrosField.text!)!
            gallL = (globalGallineroVC?.gallineroLarvasField.text!)!
            gallA = (globalGallineroVC?.gallineroAdultosField.text!)!
            gallH = (globalGallineroVC?.gallineroHuevosField.text!)!
            gallN = (globalGallineroVC?.gallineroNinfasField.text!)!
        }
        if globalCorralVC != nil{
            corrR = (globalCorralVC?.corralRastrosField.text!)!
            corrL = (globalCorralVC?.corralLarvasField.text!)!
            corrA = (globalCorralVC?.corralAdultosField.text!)!
            corrH = (globalCorralVC?.corralHuevosField.text!)!
            corrN = (globalCorralVC?.corralNinfasField.text!)!
        }
        if globalDepositoVC != nil {
            depoR = (globalDepositoVC?.depositoRastrosField.text!)!
            depoL = (globalDepositoVC?.depositoLarvasField.text!)!
            depoA = (globalDepositoVC?.depositoAdultosField.text!)!
            depoH = (globalDepositoVC?.depositoHuevosField.text!)!
            depoN = (globalDepositoVC?.depositoNinfasField.text!)!
        }
        
        reg = RegisterPero(rastroGallineroValue: gallR, huevosGallineroValue: gallH, larvasGallineroValue: gallL, ninfasGallineroValue: gallN, adultosGallineroValue: gallA, rastroCorralValue: corrR, huevosCorralValue: corrH, larvasCorralValue: corrL, ninfasCorralValue: corrN, adultosCorralValue: corrA, rastroDepositoValue: depoR, huevosDepositoValue: depoH, larvasDepositoValue: depoL, ninfasDepositoValue: depoN, adultosDepositoValue: depoA)
        peroRegisters.append(reg)
        return reg
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if newString.count > 4 {
            return false
        }
        
        return newString.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
    }
    
    func generateWarningLabel(){
        imageView.frame = CGRect(x: 335, y: 65, width: 30, height: 30)
        view.addSubview(imageView)
    }
    
    func deleteWarningLabel(){
        globalHomeVC!.emptyRegisters()
        imageView.removeFromSuperview()
    }
    
    
}

extension RegisterPeroViewController: NetworkStatusListener {
    
    func networkStatusDidChange(status: Reachability.NetworkStatus) {
        
        
        switch status {
        case .notReachable:
            self.generateWarningLabel()
        case .reachableViaWiFi:
            self.deleteWarningLabel()
        case .reachableViaWWAN:
            self.deleteWarningLabel()
        }
    }
}
