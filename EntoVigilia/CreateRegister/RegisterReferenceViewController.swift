//
//  RegisterReferenceViewController.swift
//  EntoVigilia
//
//  Created by Juan Camilo Ruiz on 3/18/19.
//  Copyright © 2019 Juan Camilo Ruiz. All rights reserved.
//

import UIKit
import os.log
import FirebaseDatabase
import Firebase
import CoreLocation
import ReachabilitySwift
import Foundation
import AVFoundation

var globalRegisterReferenceVC: RegisterReferenceViewController?

class RegisterReferenceViewController: UIViewController, UITextFieldDelegate, CLLocationManagerDelegate{

    var referenceRegisters:[RegisterReference] = []
    var refRegistros: DatabaseReference?
    var locationManager: CLLocationManager = CLLocationManager()
    var startLocation: CLLocation!
    private var roundButton = UIButton()
    private var roundFlashButton = UIButton()
    let imageView = UIImageView(image: UIImage(named: "noWifi.png"))
    let reachability = Reachability()!
    var actualRegister: Register?
    var idExistent: String = ""
    var latitudExistent: String = ""
    var longitudExistent: String = ""
    var locationSetted = false
    var latitudLocation: Double = 0.0
    var longitudLocation: Double = 0.0
    
    @IBOutlet weak var latitudField: UITextField!
    @IBOutlet weak var longitudField: UITextField!
    @IBOutlet weak var deptoField: UITextField!
    @IBOutlet weak var municipioField: UITextField!
    @IBOutlet weak var veredaField: UITextField!
    @IBOutlet weak var idField: UITextField!
    @IBOutlet weak var referenceView: UIView!
    @IBOutlet weak var scroll_view: UIScrollView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        globalRegisterReferenceVC = self
        
        createFlashButton()
        
        latitudField!.delegate = self
        longitudField!.delegate = self
        deptoField!.delegate = self
        municipioField!.delegate = self
        veredaField!.delegate = self
        idField!.delegate = self
        
        latitudField!.tag = 0
        longitudField!.tag = 1
        deptoField!.tag = 2
        municipioField!.tag = 3
        veredaField!.tag = 4
        idField!.tag = 5
        
        if idExistent != ""{
            idField.text = idExistent
            latitudField.text = latitudExistent
            longitudField.text = longitudExistent
            latitudLocation = (latitudExistent as NSString).doubleValue
            longitudLocation = (longitudExistent as NSString).doubleValue
            setLocationValues()
        } else {
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.delegate = self
            startLocation = nil
            locationManager.stopUpdatingLocation()
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
        
        makeRounded()
        
        refRegistros = Database.database().reference()
        refRegistros!.keepSynced(true)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        checkForLocationServices()
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        var contentInset:UIEdgeInsets = self.scroll_view.contentInset
        contentInset.bottom = keyboardFrame.size.height
        scroll_view.contentInset = contentInset
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        scroll_view.contentInset = contentInset
    }
    
    func checkForLocationServices() {
        if !CLLocationManager.locationServicesEnabled() {
            showNoLocalizationMsgg()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        let latesLocation: CLLocation
        latesLocation = locations[locations.count - 1]
        latitudField!.text = String(format: "%.4f", latesLocation.coordinate.latitude)
        longitudField!.text = String(format: "%.4f", latesLocation.coordinate.longitude)
        latitudLocation = latesLocation.coordinate.latitude
        longitudLocation = latesLocation.coordinate.longitude
        setLocationValues()
    }
    
    func setLocationValues(){
        if !locationSetted {
        let location = CLLocation(latitude: latitudLocation, longitude: longitudLocation)
        self.getPlace(for: location) { placemark in
            guard let placemark = placemark else { return }

            if let departamento = placemark.administrativeArea {
                self.deptoField!.text = departamento
            }
            if let municipio = placemark.locality {
                self.municipioField!.text = municipio
            }
        }
        locationSetted = true
        }
    }
    
    func makeRounded() {
        
        referenceView.layer.cornerRadius = 5
        referenceView.layer.shadowColor = UIColor.black.cgColor
        referenceView.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        referenceView.layer.masksToBounds = false
        referenceView.layer.shadowRadius = 2.0
        referenceView.layer.shadowOpacity = 0.5
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1
        
        if let nextResponder = textField.superview?.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.roundButton.isEnabled = true
        self.roundButton.isHidden = false
        createFloatingButton()
        createFlashButton()
        ReachabilityManager.shared.addListener(listener: self)
        if reachability.currentReachabilityStatus == Reachability.NetworkStatus.reachableViaWiFi {
            deleteWarningLabel()
        } else if reachability.currentReachabilityStatus == Reachability.NetworkStatus.reachableViaWWAN {
            deleteWarningLabel()
        } else {
            generateWarningLabel()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        if roundButton.superview != nil {
            DispatchQueue.main.async {
                self.roundButton.removeFromSuperview()
                self.roundButton.isEnabled = false
                self.roundButton.isHidden = true
            }
        }
        if roundFlashButton.superview != nil {
            DispatchQueue.main.async {
                self.roundFlashButton.removeFromSuperview()
                self.roundFlashButton.isEnabled = false
                self.roundFlashButton.isHidden = true
            }
        }
        ReachabilityManager.shared.removeListener(listener: self)
    }
    
    func createFloatingButton() {
        roundButton = UIButton(type: .custom)
        roundButton.translatesAutoresizingMaskIntoConstraints = false
        roundButton.backgroundColor = UIColor(red: 11/255, green: 201/255, blue: 8/255, alpha: 1.0)
        // Make sure you replace the name of the image:
        roundButton.setImage(UIImage(named:"sendImage"), for: .normal)
        // Make sure to create a function and replace DOTHISONTAP with your own function:
        roundButton.addTarget(self, action: #selector(sendRegisterClicked), for: UIControl.Event.touchUpInside)
        // We're manipulating the UI, must be on the main thread:
        DispatchQueue.main.async {
            if let keyWindow = UIApplication.shared.keyWindow {
                keyWindow.addSubview(self.roundButton)
                NSLayoutConstraint.activate([
                    keyWindow.trailingAnchor.constraint(equalTo: self.roundButton.trailingAnchor, constant: 15),
                    keyWindow.bottomAnchor.constraint(equalTo: self.roundButton.bottomAnchor, constant: 50),
                    self.roundButton.widthAnchor.constraint(equalToConstant: 50),
                    self.roundButton.heightAnchor.constraint(equalToConstant: 50)])
            }
            // Make the button round:
            self.roundButton.layer.cornerRadius = 26
            // Add a black shadow:
            self.roundButton.layer.shadowColor = UIColor.black.cgColor
            self.roundButton.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
            self.roundButton.layer.masksToBounds = false
            self.roundButton.layer.shadowRadius = 2.0
            self.roundButton.layer.shadowOpacity = 0.5
            // Add a pulsing animation to draw attention to button:
            let scaleAnimation: CABasicAnimation = CABasicAnimation(keyPath: "transform.scale")
            scaleAnimation.duration = 0.4
            scaleAnimation.repeatCount = .greatestFiniteMagnitude
            scaleAnimation.autoreverses = true
            scaleAnimation.fromValue = 1.0;
            scaleAnimation.toValue = 1.0005;
            self.roundButton.layer.add(scaleAnimation, forKey: "scale")
            self.roundButton.showsTouchWhenHighlighted = true
        }
    }
    
    func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
    
   
    @objc func sendRegisterClicked(){
        saveRegister()
        performSegue(withIdentifier: "sendRegister", sender: self)
    }
   
    func createFlashButton() {
        roundFlashButton = UIButton(type: .custom)
        roundFlashButton.translatesAutoresizingMaskIntoConstraints = false
        // roundFlashButton.backgroundColor = .green
        // Make sure you replace the name of the image:
        roundFlashButton.setImage(UIImage(named:"flashlight"), for: .normal)
        // Make sure to create a function and replace DOTHISONTAP with your own function:
        roundFlashButton.addTarget(self, action: #selector(flashLigthTapped), for: UIControl.Event.touchUpInside)
        // We're manipulating the UI, must be on the main thread:
        DispatchQueue.main.async {
            if let keyWindow = UIApplication.shared.keyWindow {
                keyWindow.addSubview(self.roundFlashButton)
                NSLayoutConstraint.activate([
                    keyWindow.trailingAnchor.constraint(equalTo: self.roundFlashButton.trailingAnchor, constant: 15),
                    keyWindow.bottomAnchor.constraint(equalTo: self.roundFlashButton.bottomAnchor, constant: 600),
                    self.roundFlashButton.widthAnchor.constraint(equalToConstant: 45),
                    self.roundFlashButton.heightAnchor.constraint(equalToConstant: 45)])
            }
            // Make the button round:
            self.roundFlashButton.layer.cornerRadius = 26
        }
    }
    
    @objc func flashLigthTapped() {
        guard let device = AVCaptureDevice.default(for: AVMediaType.video) else { return }
        guard device.hasTorch else { return }
        
        do {
            try device.lockForConfiguration()
            
            if (device.torchMode == AVCaptureDevice.TorchMode.on) {
                device.torchMode = AVCaptureDevice.TorchMode.off
            } else {
                do {
                    try device.setTorchModeOn(level: 1.0)
                } catch {
                    print(error)
                }
            }
            
            device.unlockForConfiguration()
        } catch {
            print(error)
        }
    }
    
    @objc func saveRegister() {
        var refReg: RegisterReference?
        refReg = createReferenceRegister()
        if refReg != nil{
        var peroReg: RegisterPero
        if globalRegisterPeroVC != nil{
            peroReg = globalRegisterPeroVC!.createPeroRegister()
            globalRegisterPeroVC!.saveImages(id: refReg!.id)
            globalRegisterPeroVC!.saveAudios(id: refReg!.id)
        }
        else {
            peroReg = createEmptyPeroReg()
        }
        var interReg: RegisterInter = createEmptyInterReg()
        if globalRegisterInterVC != nil {
            interReg = globalRegisterInterVC!.createInterRegister()
            globalRegisterInterVC!.saveImages(id: refReg!.id)
            globalRegisterInterVC!.saveAudios(id: refReg!.id)
        }
        else {
            interReg = createEmptyInterReg()
            }
        var register: Register
        register = Register(peroRegister:peroReg, interRegister:interReg, refRegister: refReg!)
        actualRegister = register
        }
    }
    
    
    
    func updateRegisters(register: Register){
        if Auth.auth().currentUser != nil && Auth.auth().currentUser!.isEmailVerified{
                sendFireBase(register: register)
        }
        else{
            var toSendRegistersSaved: [Register]?
            toSendRegistersSaved = retrieveToSendRegisters()
            if toSendRegistersSaved != nil {
                toSendRegistersSaved!.append(register)
            } else {
                toSendRegistersSaved = [register]
            }
            self.saveToSendRegisters(regs: toSendRegistersSaved!)
        }
        var registersSaved: [Register]?
        registersSaved = retrieveRegisters()
        registersSaved!.append(register)
        self.saveRegisters(regs: registersSaved!)
    }
    
    private func createEmptyInterReg() -> RegisterInter{
        var interReg: RegisterInter
        let valueAll = "0"
        interReg = RegisterInter(rastroCocinaValue: valueAll, huevosCocinaValue: valueAll, larvasCocinaValue: valueAll, ninfasCocinaValue: valueAll, adultosCocinaValue: valueAll, rastroDormitorioValue: valueAll, huevosDormitorioValue: valueAll, larvasDormitorioValue: valueAll, ninfasDormitorioValue: valueAll, adultosDormitorioValue: valueAll, rastroZonaSocialValue: valueAll, huevosZonaSocialValue: valueAll, larvasZonaSocialValue: valueAll, ninfasZonaSocialValue: valueAll, adultosZonaSocialValue: valueAll)
        return interReg
    }
    
    private func createEmptyPeroReg() -> RegisterPero{
        var peroReg: RegisterPero
        let valueAll = "0"
        peroReg = RegisterPero(rastroGallineroValue: valueAll, huevosGallineroValue: valueAll, larvasGallineroValue: valueAll, ninfasGallineroValue: valueAll, adultosGallineroValue: valueAll, rastroCorralValue: valueAll, huevosCorralValue: valueAll, larvasCorralValue: valueAll, ninfasCorralValue: valueAll, adultosCorralValue: valueAll, rastroDepositoValue: valueAll, huevosDepositoValue: valueAll, larvasDepositoValue: valueAll, ninfasDepositoValue: valueAll, adultosDepositoValue: valueAll)
        return peroReg
    }
    
    func sendFireBase(register: Register){
        let ref = register.refRegister
        let mapName = getSelectedMap()
        
        let referencia = [
            "latitud": Double(ref.latitud),
            "longitud":Double(ref.longitud),
            "departamento":ref.departamento,
            "municipio":ref.municipio,
            "vereda":ref.vereda,
            "id":ref.id,
            "date":ref.date,
            "diaSemana": ref.diaSemana
            ] as [String : Any]
        
        let inter = register.interRegister
        let cocinaDict = [
            "rastros": Int(inter.rastroCocinaValue),
            
            "huevos":  Int(inter.huevosCocinaValue),
            
            "larvas":  Int(inter.larvasCocinaValue),
            
            "ninfas":  Int(inter.ninfasCocinaValue),
            
            "adultos": Int(inter.adultosCocinaValue)
        ]
        let dormitorioDict = [
            "rastros": Int(inter.rastroDormitorioValue),
            
            "huevos":  Int(inter.huevosDormitorioValue),
            
            "larvas":  Int(inter.larvasDormitorioValue),
            
            "ninfas":  Int(inter.ninfasDormitorioValue),
            
            "adultos": Int(inter.adultosDormitorioValue)
        ]
        let zonaSocialDict = [
            "rastros": Int(inter.rastroZonaSocialValue),
            
            "huevos":  Int(inter.huevosZonaSocialValue),
            
            "larvas":  Int(inter.larvasZonaSocialValue),
            
            "ninfas":  Int(inter.ninfasZonaSocialValue),
            
            "adultos": Int(inter.adultosZonaSocialValue)
        ]
        let pero = register.peroRegister
        let gallineroDict = [
            "rastros": Int(pero.rastroGallineroValue),
            
            "huevos":  Int(pero.huevosGallineroValue),
            
            "larvas":  Int(pero.larvasGallineroValue),
            
            "ninfas":  Int(pero.ninfasGallineroValue),
            
            "adultos": Int(pero.adultosGallineroValue)
        ]
        
        let corralDict = [
            "rastros": Int(pero.rastroCorralValue),
            
            "huevos":  Int(pero.huevosCorralValue),
            
            "larvas":  Int(pero.larvasCorralValue),
            
            "ninfas":  Int(pero.ninfasCorralValue),
            
            "adultos": Int(pero.adultosCorralValue)
        ]
        let depositoDict = [
            "rastros": Int(pero.rastroDepositoValue),
            
            "huevos":  Int(pero.huevosDepositoValue),
            
            "larvas":  Int(pero.larvasDepositoValue),
            
            "ninfas":  Int(pero.ninfasDepositoValue),
            
            "adultos": Int(pero.adultosDepositoValue)
        ]
        let intradomicilio = [
            "cocina":cocinaDict,
            "dormitorio":dormitorioDict,
            "zonaSocial":zonaSocialDict
            ] as [String : Any]
        let periDomicilio = [
            "gallinero":gallineroDict,
            "corral":corralDict,
            "deposito":depositoDict
        ]
        
        let registro = [
            "peridomicilio":periDomicilio,
            "intradomicilio":intradomicilio,
            "referencia":referencia
            ]  as [String : Any]
        
        let pinRegistro = [
            "idHogar":ref.id,
            "latitud":Double(ref.latitud),
            "longitud":Double(ref.longitud),
            "registrado":true
        ] as [String : Any]
        
        let pinHistoryRegister = [
            "idHogar":ref.id,
            "latitud":Double(ref.latitud),
            "longitud":Double(ref.longitud),
            "fecha": ref.date,
            "departamento": ref.departamento,
            "municipio": ref.municipio,
            "vereda": ref.vereda
        ] as [String : Any]
        
        self.refRegistros?.child("registros").childByAutoId().setValue(registro)
        if mapName != ""{
        self.refRegistros?.child("mapas").child(mapName).child("hogares").child((ref.id)).setValue(pinRegistro)
        } else {
            print("No se pudo enviar el registro a firebase mapas porque el nombre del mapa es vacio")
            return
        }
        if Auth.auth().currentUser != nil{
            let username = removeInvalidChars(str: Auth.auth().currentUser!.email!)
            self.refRegistros?.child("usuarios").child(username).child(ref.id).setValue(pinHistoryRegister)
        }
    }
    
    func removeInvalidChars(str: String) -> String {
        return str.replacingOccurrences(of: ".", with: "", options: NSString.CompareOptions.literal, range: nil).replacingOccurrences(of: "@", with: "", options: NSString.CompareOptions.literal, range: nil).replacingOccurrences(of: "#", with: "", options: NSString.CompareOptions.literal, range: nil).replacingOccurrences(of: "$", with: "", options: NSString.CompareOptions.literal, range: nil).replacingOccurrences(of: "[", with: "", options: NSString.CompareOptions.literal, range: nil).replacingOccurrences(of: "]", with: "", options: NSString.CompareOptions.literal, range: nil)
    }
    
    func getSelectedMap() -> String {
        if UserDefaults.standard.object(forKey: "mapName") != nil {
            for dictionary in UserDefaults.standard.object(forKey: "mapName") as! [[String:Any]]{
                return dictionary["name"] as! String
            }
        }
        return ""
    }
    
    private func saveRegisters(regs: [Register]) {
        do {
            let data = try PropertyListEncoder().encode(regs)
            let success = NSKeyedArchiver.archiveRootObject(data, toFile: Register.ArchiveURL.path)
            print(success ? "Successful save" : "Save Failed")
        } catch {
            print("Save Failed")
        }
    }
    
    private func saveToSendRegisters(regs: [Register]) {
        do {
            let data = try PropertyListEncoder().encode(regs)
            let success = NSKeyedArchiver.archiveRootObject(data, toFile: Register.ToSendArchiveURL.path)
            print(success ? "Successful save" : "Save Failed")
        } catch {
            print("Save Failed")
        }
    }
    
    func retrieveRegisters() -> [Register]? {
        guard let data = NSKeyedUnarchiver.unarchiveObject(withFile: Register.ArchiveURL.path) as? Data else { return nil }
        do {
            let registers = try PropertyListDecoder().decode([Register].self, from: data)
            return registers
        } catch {
            print("Retrieve Failed")
            return nil
        }
    }
    
    func retrieveToSendRegisters() -> [Register]? {
    guard let data = NSKeyedUnarchiver.unarchiveObject(withFile: Register.ToSendArchiveURL.path) as? Data else { return nil }
    do {
        let registers = try PropertyListDecoder().decode([Register].self, from: data)
        return registers
        } catch {
            print("Retrieve Failed")
            return nil
        }
    }
    
    func createReferenceRegister() -> RegisterReference?{
        var reg : RegisterReference? = nil
        
        guard let lat = latitudField.text, !lat.isEmpty else {
            displayMyAlertMessage (userMessage: "Olvidaste ingresar la latitud en la Referencia")
            return nil
        }
        
        guard let long = longitudField.text, !long.isEmpty else {
            displayMyAlertMessage (userMessage: "Olvidaste ingresar la longitud en la Referencia")
            return nil
        }
        
        guard let depto = deptoField.text, !depto.isEmpty else {
            displayMyAlertMessage (userMessage: "Olvidaste ingresar el Departamento en la Referencia")
            return nil
        }
        guard let municpio = municipioField.text, !municpio.isEmpty else {
            displayMyAlertMessage (userMessage: "Olvidaste ingresar el Municipio en la Referencia")
            return nil
        }
        guard let vereda = veredaField.text, !vereda.isEmpty else {
            displayMyAlertMessage (userMessage: "Olvidaste ingresar la Vereda en la Referencia")
            return nil
        }
        guard let id = idField.text, !id.isEmpty else {
            displayMyAlertMessage (userMessage: "Olvidaste ingresar el ID en la Referencia")
            return nil
        }
        reg = RegisterReference(latitud: latitudField!.text!, longitud: longitudField!.text!, departamento: deptoField!.text!, municipio:municipioField!.text!, vereda: veredaField!.text!, id: (idField!.text!))
        referenceRegisters.append(reg!)
        return reg
    }
    
    func displayCreatedMessage(id: String){
        let userMessage: String = "¿ Estas seguro que deseas crear el registro con id " + id + " ?"
        let myAlert = UIAlertController(title: "¿Crear registro?", message: userMessage, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "Si, estoy seguro", style: UIAlertAction.Style.default, handler: {_ in
            let VC = self.storyboard?.instantiateViewController(withIdentifier: "saveRegisterView") as! SaveRegisterViewController
            self.present(VC, animated: true, completion: nil)
        })
        let cancelAction = UIAlertAction(title: "No, cancelar", style: .cancel) { (action:UIAlertAction!) in
        }
        myAlert.addAction(cancelAction)
        myAlert.addAction(okAction)
        self.present(myAlert, animated: true, completion: nil)
    }
    
    func showNoLocalizationMsgg(){
        let alert = UIAlertController(title: "No hay localización", message: "Se deben ingresar la latitud y longitud manualmente", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func displayMyAlertMessage(userMessage:String){
        
        let myAlert = UIAlertController(title: "Imposible crear el registro", message: userMessage, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil)
        myAlert.addAction(okAction)
        self.present(myAlert, animated: true, completion: nil)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if newString.count > 25 {
            return false
        }
        return true
    }
    
    func generateWarningLabel(){
        imageView.frame = CGRect(x: 335, y: 65, width: 30, height: 30)
        view.addSubview(imageView)
    }
    
    func deleteWarningLabel(){
        globalHomeVC!.emptyRegisters()
        imageView.removeFromSuperview()
    }
    
}
extension RegisterReferenceViewController: NetworkStatusListener {
    
    func networkStatusDidChange(status: Reachability.NetworkStatus) {
        
        switch status {
        case .notReachable:
            self.generateWarningLabel()
        case .reachableViaWiFi:
            self.deleteWarningLabel()
        case .reachableViaWWAN:
            self.deleteWarningLabel()
        }
 
    }
}

extension RegisterReferenceViewController {
    
    func getPlace(for location: CLLocation,
                  completion: @escaping (CLPlacemark?) -> Void) {
        
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(location) { placemarks, error in
            
            guard error == nil else {
                print("*** Error in \(#function): \(error!.localizedDescription)")
                completion(nil)
                return
            }
            
            guard let placemark = placemarks?[0] else {
                print("*** Error in \(#function): placemark is nil")
                completion(nil)
                return
            }
            
            completion(placemark)
        }
    }
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
