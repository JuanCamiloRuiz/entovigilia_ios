//
//  createFBTableViewCell.swift
//  EntoVigilia
//
//  Created by Juan Camilo Ruiz on 4/3/19.
//  Copyright © 2019 Juan Camilo Ruiz. All rights reserved.
//

import UIKit

class createFBTableViewCell: UITableViewCell {

    
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var latitudLabel: UILabel!
    @IBOutlet weak var longitudLabel: UILabel!
    @IBOutlet weak var createButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
